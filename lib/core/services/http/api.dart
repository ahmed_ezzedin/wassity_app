import 'package:wassity/core/services/core_helper/preference.dart';
import 'dart:ui' as ui;

enum RequestType {
  post,
  put,
  get,
  delete,
}

class Headers {
  static String? languageCode = Preference.getString(PrefKeys.languageCode) ??
      ui.window.locale.languageCode;

  static Map<String, String> get clientAuth => {
        "Accept": "application/json",
        "content-type": "application/json",
        "language": "$languageCode",
        "Authorization": "bearer token",
      };
  static Map<String, String> get userAuth => {
        "Accept": "application/json",
        "content-type": "application/json",
        "language": "$languageCode",
        "Authorization": "bearer ${Preference.getString(PrefKeys.token)}",
      };

}

class EndPoints {
  // static const String serverURL = "http://admin.wasety.net/api/";
  static const String serverURL = "https://admin.wasety.net/api/";
  static const String login = "auth/login";
  static const String signin = "auth/register";
  static const String country = "country";
  static const String city = "cities";
  static const String changePassword = "changePassword";
  static const String forgetPassword = "auth/forgot";
  static const String allUsers = "users";
  static const String notifications = "notifications";
  static const String projects = "projects";
  static const String updateProfile = "profile";
  static const String products = "products";
  static const String info = "info";
  static const String offers = "offers";
  static const String hire = "offers/hire";
  static const String rate = "rate";


}
