import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:wassity/core/services/http/api.dart';
import 'package:wassity/core/services/provider/provider_setup.dart';
import 'package:wassity/view/pages/splash_page.dart';
import 'package:wassity/view/view_helper/app_helper.dart';
import 'package:wassity/view/view_helper/dialog_helper.dart';

class HttpService {
  // =========================================================== Main Request Function
  Future<dynamic> request(
    BuildContext context, {
    required String endPoint,
    required RequestType requestType,
    required Map<String, String> header,
    Map<String, dynamic>? body,
    Map<String, dynamic>? queryParams,
  }) async {
    http.Response? response;
    Duration timeoutDuration = const Duration(seconds: 30);
    Uri uri = Uri.parse(EndPoints.serverURL + endPoint).replace(queryParameters: queryParams);
    try {
      switch (requestType) {
        case RequestType.post:
          {
            response = await http
                .post(
                  uri,
                  headers: header,
                  encoding: Encoding.getByName("utf-8"),
                  body: json.encode(body),
                )
                .timeout(timeoutDuration);
          }
          break;
        case RequestType.put:
          {
            response = await http
                .put(
                  uri,
                  headers: header,
                  encoding: Encoding.getByName("utf-8"),
                  body: json.encode(body),
                )
                .timeout(timeoutDuration);
          }
          break;
        case RequestType.get:
          {
            response = await http
                .get(
                  uri,
                  headers: header,
                )
                .timeout(timeoutDuration);
          }
          break;
        case RequestType.delete:
          {
            response = await http
                .delete(
                  uri,
                  headers: header,
                )
                .timeout(timeoutDuration);
          }
          break;
      }
      if (response.statusCode == 200 || response.statusCode == 201) {
        AppHelper.appPrint("Request Success ✅");
      } else if (response.statusCode == 401) {
        auth.signOut();
        AppHelper.pushReplaceAll(context, const SplashPage());
      } else {
        AppHelper.appPrint("Request Failed Code(${response.statusCode}) ❌");
      }
    } on TimeoutException catch (error) {
      AppHelper.appPrint("TimeoutException >>> ❗ ${error.toString()} ❗");
      DialogHelper.errorDialog(context, message: error.toString());
    } on SocketException catch (error) {
      AppHelper.appPrint("SocketException >>> ❗ ${error.toString()} ❗");
      DialogHelper.errorDialog(context, message: error.toString());
    } catch (error) {
      AppHelper.appPrint("🚨 ${error.toString()} 🚨");
      AppHelper.printObject("===================================");
      AppHelper.printObject(response!.body);
    }
    if (response != null) {
      dynamic stringJson = json.decode((response.body));
      AppHelper.printObject(stringJson);
      return stringJson;
    } else {
      return null;
    }
  }

  Future<dynamic> getAllCountries(BuildContext context) async {
    final res = await request(
      context,
      endPoint: EndPoints.country,
      requestType: RequestType.get,
      header: Headers.clientAuth,
    );
    return res;
  }

  Future<dynamic> getAllCities(BuildContext context) async {
    final res = await request(
      context,
      endPoint: EndPoints.city,
      requestType: RequestType.get,
      header: Headers.clientAuth,
    );
    return res;
  }

  Future<dynamic> getAllUsers(BuildContext context) async {
    final res = await request(
      context,
      endPoint: EndPoints.allUsers,
      requestType: RequestType.get,
      header: Headers.userAuth,
    );
    return res;
  }

  Future<dynamic> getAllProjects(BuildContext context) async {
    final res = await request(
      context,
      endPoint: EndPoints.projects,
      requestType: RequestType.get,
      header: Headers.userAuth,
    );
    return res;
  }

  Future<dynamic> getProjectsIndex(BuildContext context, {required Map<String, dynamic> body}) async {
    final res = await request(
      context,
      endPoint: EndPoints.projects,
      requestType: RequestType.get,
      header: Headers.userAuth,
      // queryParams: queryParams
      body: body,
    );
    AppHelper.printObject(res);
    return res;
  }

  Future<dynamic> getAllNotifications(BuildContext context) async {
    final res = await request(
      context,
      endPoint: EndPoints.notifications,
      requestType: RequestType.get,
      header: Headers.userAuth,
    );
    return res;
  }

  Future<dynamic> addProject(
    BuildContext context, {
    required Map<String, dynamic> body,
  }) async {
    final res = await request(
      context,
      endPoint: EndPoints.projects,
      requestType: RequestType.post,
      header: Headers.userAuth,
      body: body,
    );
    return res;
  }

  Future<dynamic> updateProfile(
    BuildContext context, {
    required Map<String, dynamic> body,
  }) async {
    final res = await request(
      context,
      endPoint: EndPoints.updateProfile,
      requestType: RequestType.post,
      header: Headers.userAuth,
      body: body,
    );
    return res;
  }

  Future<dynamic> getAllProducts(BuildContext context) async {
    final res = await request(
      context,
      endPoint: EndPoints.products,
      requestType: RequestType.get,
      header: Headers.userAuth,
    );
    return res;
  }

  Future<dynamic> getInfo(BuildContext context) async {
    final res = await request(
      context,
      endPoint: EndPoints.info,
      requestType: RequestType.get,
      header: Headers.userAuth,
    );
    return res;
  }

  Future<dynamic> createOffer(
    BuildContext context, {
    required Map<String, dynamic> body,
  }) async {
    final res = await request(
      context,
      endPoint: EndPoints.offers,
      requestType: RequestType.post,
      header: Headers.userAuth,
      body: body,
    );
    return res;
  }

  Future<dynamic> hireOffer(
    BuildContext context, {
    required Map<String, dynamic> body,
  }) async {
    final res = await request(
      context,
      endPoint: EndPoints.hire,
      requestType: RequestType.post,
      header: Headers.userAuth,
      body: body,
    );
    return res;
  }

  Future<dynamic> getRateIndex(
    BuildContext context, {
    required Map<String, dynamic> body,
  }) async {
    final res = await request(
      context,
      endPoint: EndPoints.rate,
      requestType: RequestType.get,
      header: Headers.userAuth,
      body: body,
    );
    return res;
  }
}
