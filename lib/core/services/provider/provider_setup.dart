import 'package:get_it/get_it.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:wassity/core/services/auth/auth_service.dart';
import 'package:wassity/core/services/http/http_service.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/core/services/theme/app_theme.dart';

final GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => HttpService());
  locator.registerLazySingleton(() => AuthService());
}

  HttpService api = locator<HttpService>();
  AuthService auth = locator<AuthService>();


List<SingleChildWidget> providers = [
  ...servicesProviders,
  ...helpersProviders,
];

List<SingleChildWidget> servicesProviders = [
  ChangeNotifierProvider(create: (_) => AppLanguage()),
  ChangeNotifierProvider(create: (_) => AppTheme()),
];
List<SingleChildWidget> helpersProviders = [
  
  // ChangeNotifierProvider(create: (_) => UserProvider()),
];
