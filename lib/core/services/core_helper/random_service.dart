import 'dart:math';
import 'package:flutter/material.dart';

class RandomService {
  static Random random =  Random();

//==================================== Generate random int number from 0  to max
  static int randomIntNumber(int max) {
    return random.nextInt(max);
  }

//==================================== Generate random double number from 0.0  to 1.0
  static double randomDoubleNumber() {
    return random.nextDouble();
  }

//==================================== Generate random Color
  static Color randomColor() {
    int number = random.nextInt(0xffffffff);
    return Color(number);
  }
}
