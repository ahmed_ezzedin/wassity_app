import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:wassity/view/view_helper/app_helper.dart';

class FileHelper {


//=========================================================  Pick Image
  static Future<File?> pickImage(ImageSource source) async {
    try {
      XFile? xFile = await ImagePicker().pickImage(
        source: source,
      );
      if (xFile == null) {
        AppHelper.showToastMesssage("No images selected.");
        return null;
      }
      return File(xFile.path);
    } catch (error) {
      // ignore: avoid_print
      print(error);
    }
  }

//=========================================================  Pick Images List
  static Future<List<File>?> pickMultiImage() async {
    try {
      List<XFile>? xFileList = await ImagePicker().pickMultiImage();
      if (xFileList == null) {
        AppHelper.showToastMesssage("No images selected.");
        return null;
      }
      final List<File> filesList = [];
      for (var element in xFileList) {
        filesList.add(File(element.path));
      }
      return filesList;
    } catch (error) {
      // ignore: avoid_print
      print(error);
    }
  }

}
