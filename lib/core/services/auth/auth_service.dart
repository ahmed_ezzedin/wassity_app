import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:wassity/core/models/user_model.dart';
import 'package:wassity/core/services/core_helper/preference.dart';
import 'package:wassity/core/services/http/api.dart';
import 'package:wassity/core/services/provider/provider_setup.dart';
import 'package:wassity/view/view_helper/app_helper.dart';
import 'package:wassity/view/view_helper/dialog_helper.dart';

class AuthService extends ChangeNotifier {
  UserModel? _userModel;
  DateTime? loginTime;
  UserModel? get userModel => _userModel;
  bool get isLogin => Preference.getBool(PrefKeys.isUserLoged) ?? false;
  late Timer expireTimer;
  saveUser(UserModel userModel) async {
    await Preference.setString(
        PrefKeys.userModel, json.encode(userModel.toJson()));
    await Preference.setString(PrefKeys.token, userModel.accessToken!);
    await Preference.setBool(PrefKeys.isUserLoged, true);
    _userModel = userModel;
    AppHelper.printObject(_userModel);
    notifyListeners();
  }

  loadUser() {
    Map<String, dynamic> userMap =
        json.decode(Preference.getString(PrefKeys.userModel)!);
    _userModel = UserModel.fromJson(userMap);
    AppHelper.printObject(_userModel);
  }

  Future<void> signOut() async {
    await Preference.remove(PrefKeys.userModel);
    await Preference.remove(PrefKeys.token);
    await Preference.remove(PrefKeys.isUserLoged);
    _userModel = null;
  }

  Future<bool> checkToken(int expireSeconds) async {
    DateTime timeNow =
        DateTime.parse(Preference.getString(PrefKeys.loginTime)?? DateTime.now().toIso8601String());
    DateTime expireTime = timeNow.add(Duration(seconds: expireSeconds));
    AppHelper.printText("TimeNow    : $timeNow");
    AppHelper.printText("ExpireTime : $expireTime");
    if (expireTime.isAfter(timeNow)) {
      AppHelper.printText("****************************************");
      AppHelper.printText("Token is Not Expire");
      int expireIn = expireTime.difference(timeNow).inSeconds;
      AppHelper.printText("$expireIn");
      expireTimer = Timer(
        Duration(seconds: expireIn),
        () {
          AppHelper.printText("****************************************");
          AppHelper.printText("Token Expire");
          signOut();

        },
      );
      return false;
    } else {
      AppHelper.printText("****************************************");
      AppHelper.printText("Token is Expire");
      return true;
    }
  }


  Future<bool> signIn(
    BuildContext context, {
    required Map<String, dynamic> body,
  }) async {
    final res = await api.request(
      context,
      endPoint: EndPoints.login,
      requestType: RequestType.post,
      header: Headers.clientAuth,
      body: body,
    );
    if (res["message"] != null) {
      DialogHelper.errorDialog(context, message: res["message"]);
    }
    if (res["access_token"] != null) {
      UserModel user = UserModel.fromJson(res);
      loginTime = DateTime.now();
      await Preference.setString(
          PrefKeys.loginTime, loginTime!.toIso8601String());
      saveUser(user);
      await Future.delayed(const Duration(milliseconds: 1000));
      return true;
    }
    return false;
  }

  Future<bool> signup(
    BuildContext context, {
    required Map<String, dynamic> body,
  }) async {
    final res = await api.request(
      context,
      endPoint: EndPoints.signin,
      requestType: RequestType.post,
      header: Headers.clientAuth,
      body: body,
    );
    if (res["message"] != null) {
      DialogHelper.errorDialog(context, message: "${res["message"][0]}");
    }
    if (res["access_token"] != null) {
      UserModel user = UserModel.fromJson(res);
      loginTime = DateTime.now();
      await Preference.setString(
          PrefKeys.loginTime, loginTime!.toIso8601String());
      saveUser(user);
      return true;
    }
    return false;
  }

  Future<bool> changePassword(
    BuildContext context, {
    required Map<String, dynamic> body,
  }) async {
    final res = await api.request(
      context,
      endPoint: EndPoints.changePassword,
      requestType: RequestType.put,
      header: Headers.userAuth,
      body: body,
    );
    if (res["success"] != null) {
      AppHelper.showSnackBarMessage(context, message: "${res["message"]}");
      return true;
    } else {
      AppHelper.showSnackBarMessage(context, message: "${res["message"]}");
    }
    return false;
  }

  Future<bool> forgetPassword(
    BuildContext context, {
    required Map<String, dynamic> body,
  }) async {
    final res = await api.request(
      context,
      endPoint: EndPoints.forgetPassword,
      requestType: RequestType.post,
      header: Headers.userAuth,
      body: body,
    );
    if (res["success"] == true) {
      AppHelper.showSnackBarMessage(context, message: "${res["message"]}");
      return true;
    } else {
      AppHelper.showSnackBarMessage(context, message: "${res["message"]}");
    }
    return false;
  }

  // Future<bool> updateUser(BuildContext context, {required Map<String, dynamic> body}) async {
  //   final res = await api.request(
  //     endPoint: EndPoints.Update_Profile,
  //     requestType: RequestType.Post,
  //     header: Headers.userAuth,
  //     body: body,
  //   );
  //   if (res["data"] != null) {
  //     User user = User.fromJson(res["data"]);
  //     saveUser(user);
  //     return true;
  //   } else {
  //     if (res["check"] == false) {
  //       DialogHelper.errorDialog(context, message: res["msg"]);
  //     }
  //   }
  //   return false;
  // }
}
