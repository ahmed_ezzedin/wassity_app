import 'package:wassity/core/models/user_model.dart';

class Project {
  int? id;
  String? title;
  String? description;
  String? rate;
  String? image;
  String? amount;
  String? status;
  User? user;

  Project(
      {this.id,
      this.title,
      this.description,
      this.rate,
      this.image,
      this.amount,
      this.status,
      this.user});

  Project.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    description = json['description'];
    rate = json['rate'];
    image = json['image'];
    amount = json['amount'];
    status = json['status'];
    user = json['user'] != null ? User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['description'] = description;
    data['rate'] = rate;
    data['image'] = image;
    data['amount'] = amount;
    data['status'] = status;
    if (user != null) {
      data['user'] = user!.toJson();
    }
    return data;
  }
}

