class NotificationModel {
  int? id;
  String? title;
  String? type;
  Data? data;

  NotificationModel({this.id, this.title, this.type, this.data});

  NotificationModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    type = json['type'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['type'] = type;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  int? id;
  String? title;
  String? photo;
  String? description;
  String? userId;
  String? amount;
  String? status;
  String? active;
  String? deletedAt;
  String? createdAt;
  String? updatedAt;
  String? offerId;
  String? rate;
  String? image;

  Data(
      {this.id,
      this.title,
      this.photo,
      this.description,
      this.userId,
      this.amount,
      this.status,
      this.active,
      this.deletedAt,
      this.createdAt,
      this.updatedAt,
      this.offerId,
      this.rate,
      this.image});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    photo = json['photo'];
    description = json['description'];
    userId = json['user_id'];
    amount = json['amount'];
    status = json['status'];
    active = json['active'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    offerId = json['offer_id'];
    rate = json['rate'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['photo'] = photo;
    data['description'] = description;
    data['user_id'] = userId;
    data['amount'] = amount;
    data['status'] = status;
    data['active'] = active;
    data['deleted_at'] = deletedAt;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['offer_id'] = offerId;
    data['rate'] = rate;
    data['image'] = image;
    return data;
  }
}
