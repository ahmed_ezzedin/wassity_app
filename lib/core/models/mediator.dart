import 'package:wassity/core/models/city.dart';
import 'package:wassity/core/models/country.dart';
import 'package:wassity/core/models/user_model.dart';

class Mediator {
  int? id;
  String? firstName;
  String? lastName;
  String? email;
  String? phone;
  String? coinPrice;
  String? rate;
  String? type;
  String? image;
  String? bio;
  String? active;
  Country? country;
  City? city;
  CreateDates ?createDates;
  UpdateDates? updateDates;

  Mediator(
      {this.id,
      this.firstName,
      this.lastName,
      this.email,
      this.phone,
      this.coinPrice,
      this.rate,
      this.type,
      this.image,
      this.bio,
      this.active,
      this.country,
      this.city,
      this.createDates,
      this.updateDates});

  Mediator.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    email = json['email'];
    phone = json['phone'];
    coinPrice = json['coin_price'];
    rate = json['rate'];
    type = json['type'];
    image = json['image'];
    bio = json['bio'];
    active = json['active'];
    country =
        json['country'] != null ? Country.fromJson(json['country']) : null;
    city = json['city'] != null ? City.fromJson(json['city']) : null;
    createDates = json['create_dates'] != null
        ? CreateDates.fromJson(json['create_dates'])
        : null;
    updateDates = json['update_dates'] != null
        ? UpdateDates.fromJson(json['update_dates'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['first_name'] = firstName;
    data['last_name'] = lastName;
    data['email'] = email;
    data['phone'] = phone;
    data['coin_price'] = coinPrice;
    data['rate'] = rate;
    data['type'] = type;
    data['image'] = image;
    data['bio'] = bio;
    data['active'] = active;
    if (country != null) {
      data['country'] = country!.toJson();
    }
    if (city != null) {
      data['city'] = city!.toJson();
    }
    if (createDates != null) {
      data['create_dates'] = createDates!.toJson();
    }
    if (updateDates != null) {
      data['update_dates'] = updateDates!.toJson();
    }
    return data;
  }
}


