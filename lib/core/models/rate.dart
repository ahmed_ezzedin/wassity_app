class Rate {
  int? id;
  String? value;
  String? message;
  String? photo;
  UserId? userId;
  CreateDates? createDates;
  UpdateDates? updateDates;

  Rate(
      {this.id,
      this.value,
      this.message,
      this.photo,
      this.userId,
      this.createDates,
      this.updateDates});

  Rate.fromJson(Map<String?, dynamic> json) {
    id = json['id'];
    value = json['value'];
    message = json['message'];
    photo = json['photo'];
    userId =
        json['user_id'] != null ? UserId.fromJson(json['user_id']) : null;
    createDates = json['create_dates'] != null
        ? CreateDates.fromJson(json['create_dates'])
        : null;
    updateDates = json['update_dates'] != null
        ? UpdateDates.fromJson(json['update_dates'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['value'] = value;
    data['message'] = message;
    data['photo'] = photo;
    if (userId != null) {
      data['user_id'] = userId!.toJson();
    }
    if (createDates != null) {
      data['create_dates'] = createDates!.toJson();
    }
    if (updateDates != null) {
      data['update_dates'] = updateDates!.toJson();
    }
    return data;
  }
}

class UserId {
  int? id;
  String? firstName;
  String? lastName;
  String? email;
  String? phone;
  String? coinPrice;
  String? rate;
  String? type;
  String? image;
  String? bio;
  String? active;
  String? country;
  String? city;
  CreateDates? createDates;
  UpdateDates? updateDates;

  UserId(
      {this.id,
      this.firstName,
      this.lastName,
      this.email,
      this.phone,
      this.coinPrice,
      this.rate,
      this.type,
      this.image,
      this.bio,
      this.active,
      this.country,
      this.city,
      this.createDates,
      this.updateDates});

  UserId.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    email = json['email'];
    phone = json['phone'];
    coinPrice = json['coin_price'];
    rate = json['rate'];
    type = json['type'];
    image = json['image'];
    bio = json['bio'];
    active = json['active'];
    country = json['country'];
    city = json['city'];
    createDates = json['create_dates'] != null
        ? CreateDates.fromJson(json['create_dates'])
        : null;
    updateDates = json['update_dates'] != null
        ? UpdateDates.fromJson(json['update_dates'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['first_name'] = firstName;
    data['last_name'] = lastName;
    data['email'] = email;
    data['phone'] = phone;
    data['coin_price'] = coinPrice;
    data['rate'] = rate;
    data['type'] = type;
    data['image'] = image;
    data['bio'] = bio;
    data['active'] = active;
    data['country'] = country;
    data['city'] = city;
    if (createDates != null) {
      data['create_dates'] = createDates!.toJson();
    }
    if (updateDates != null) {
      data['update_dates'] = updateDates!.toJson();
    }
    return data;
  }
}

class CreateDates {
  String? createdAtHuman;
  String? createdAt;

  CreateDates({this.createdAtHuman, this.createdAt});

  CreateDates.fromJson(Map<String, dynamic> json) {
    createdAtHuman = json['created_at_human'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['created_at_human'] = createdAtHuman;
    data['created_at'] = createdAt;
    return data;
  }
}

class UpdateDates {
  String? updatedAtHuman;
  String? updatedAt;

  UpdateDates({this.updatedAtHuman, this.updatedAt});

  UpdateDates.fromJson(Map<String, dynamic> json) {
    updatedAtHuman = json['updated_at_human'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['updated_at_human'] = updatedAtHuman;
    data['updated_at'] = updatedAt;
    return data;
  }
}
