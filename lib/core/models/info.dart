
class Info {
  String? websiteNameEn;
  String? websiteNameAr;
  String? mainColor;
  String? secodaryColor;
  String? logoEn;
  String? logoAr;
  String? email;
  String? phone;
  String? address;
  String? fbLink;
  String? twitterLink;
  String? linkedLink;
  String? instagramLink;
  String? bioEn;
  String? bioAr;
  String? aboutUsEn;
  String? aboutUsAr;
  String? policyEn;
  String? policyAr;
  String? privacyEn;

  Info(
      {this.websiteNameEn,
      this.websiteNameAr,
      this.mainColor,
      this.secodaryColor,
      this.logoEn,
      this.logoAr,
      this.email,
      this.phone,
      this.address,
      this.fbLink,
      this.twitterLink,
      this.linkedLink,
      this.instagramLink,
      this.bioEn,
      this.bioAr,
      this.aboutUsEn,
      this.aboutUsAr,
      this.policyEn,
      this.policyAr,
      this.privacyEn});

  Info.fromJson(Map<String, dynamic> json) {
    websiteNameEn = json['website_name_en'];
    websiteNameAr = json['website_name_ar'];
    mainColor = json['main_color'];
    secodaryColor = json['secodary_color'];
    logoEn = json['logo_en'];
    logoAr = json['logo_ar'];
    email = json['email'];
    phone = json['phone'];
    address = json['address'];
    fbLink = json['fb_link'];
    twitterLink = json['twitter_link'];
    linkedLink = json['linked_link'];
    instagramLink = json['instagram_link'];
    bioEn = json['bio_en'];
    bioAr = json['bio_ar'];
    aboutUsEn = json['about_us_en'];
    aboutUsAr = json['about_us_ar'];
    policyEn = json['policy_en'];
    policyAr = json['policy_ar'];
    privacyEn = json['privacy_en'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['website_name_en'] = websiteNameEn;
    data['website_name_ar'] = websiteNameAr;
    data['main_color'] = mainColor;
    data['secodary_color'] = secodaryColor;
    data['logo_en'] = logoEn;
    data['logo_ar'] = logoAr;
    data['email'] = email;
    data['phone'] = phone;
    data['address'] = address;
    data['fb_link'] = fbLink;
    data['twitter_link'] = twitterLink;
    data['linked_link'] = linkedLink;
    data['instagram_link'] = instagramLink;
    data['bio_en'] = bioEn;
    data['bio_ar'] = bioAr;
    data['about_us_en'] = aboutUsEn;
    data['about_us_ar'] = aboutUsAr;
    data['policy_en'] = policyEn;
    data['policy_ar'] = policyAr;
    data['privacy_en'] = privacyEn;
    return data;
  }
}
