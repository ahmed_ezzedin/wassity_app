class Product {
  int? id;
  String? name;
  String? details;
  String? image;
  String? price;

  Product({this.id, this.name, this.details, this.image, this.price});

  Product.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    details = json['details'];
    image = json['image'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['details'] = details;
    data['image'] = image;
    data['price'] = price;
    return data;
  }
}
