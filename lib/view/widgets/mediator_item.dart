import 'package:flutter/material.dart';
import 'package:wassity/core/models/mediator.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/pages/nav_bar/home_tab.dart';
import 'package:wassity/view/styles/app_colors.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/widgets/components/main_image.dart';
import 'package:wassity/view/widgets/dialogs/hire_dialog.dart';

class MediatorItem extends StatelessWidget {
  final Mediator mediator;
  final HomeTabModel model;
  const MediatorItem({
    Key? key,
    required this.mediator,
    required this.model,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final AppLocalizations locale = AppLocalizations.of(context);
    return InkWell(
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 7),
        height: SizeConfig.height * 0.15,
        width: double.infinity,
        child: Row(
          children: [
            MainImage(
              height: SizeConfig.height * 0.15,
              width: SizeConfig.height * 0.15,
              imageType: ImageType.network,
              radius: 5,
              boxFit: BoxFit.cover,
              imagePath:
                  "http://cryptoincome.io/wp-content/uploads/2018/01/DQmbtsqci8T9j9oNrQg4BxXr8hSsLYseiY788HuN7i7LfZu.jpg",
            ),
            const SizedBox(width: 10),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      mediator.image == null
                          ? Container(
                              height: 40,
                              width: 40,
                              decoration: const BoxDecoration(
                                color: Colors.grey,
                                shape: BoxShape.circle,
                              ),
                              child: const Icon(
                                Icons.person_outline,
                                color: Colors.white,
                              ),
                            )
                          : MainImage(
                              imageType: ImageType.network,
                              imagePath: "${mediator.image}",
                              isCircle: true,
                              height: 40,
                              width: 40,
                              boxFit: BoxFit.cover,
                            ),
                      const SizedBox(width: 10),
                      Text(
                        "${mediator.firstName} ${mediator.lastName}",
                        style: const TextStyle(
                          fontWeight: FontWeight.w900,
                          fontSize: 16,
                        ),
                      ),
                    ],
                  ),
                  Text(
                    // 'I Will Buy A We We Coin From You At The Largest Price And Sell It At The Largest Price',
                    "${mediator.bio}",
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(
                      color: Colors.black87,
                    ),
                  ),
                  Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      Wrap(
                        children: [
                          Icon(
                            Icons.star,
                            size: 15,
                            color: Colors.yellow.shade600,
                          ),
                          Text(
                            "${mediator.rate}",
                            style: TextStyle(
                              color: Colors.yellow.shade600,
                              fontSize: 10,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(width: 10),
                          const Text(
                            "(120)",
                            style: TextStyle(
                              fontSize: 12,
                              // fontWeight:
                              //     FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(width: 10),
                      Wrap(
                        children: [
                          Text(
                            locale.get("starting from"),
                            style: const TextStyle(
                              color: AppColors.primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            " ${mediator.coinPrice}\$",
                            style: TextStyle(
                              color: Colors.yellow.shade600,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      onTap: () {
        showDialog(
          context: context,
          builder: (ctx) {
            return HireDialog(context: context , model: model  ,mediator:mediator );
          },
        );
      },
    );
  }
}
