import 'package:flutter/material.dart';
import 'package:wassity/core/models/rate.dart';
import 'package:wassity/view/widgets/components/main_image.dart';
import 'package:wassity/view/widgets/components/stars_rating_widget.dart';

class ReviewWidget extends StatelessWidget {
  final Rate rate;
  const ReviewWidget({
    Key? key,
    required this.rate,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // MainImage(
                //   imageType: ImageType.network,
                //   imagePath: rate.userId!.image!,
                //   // imagePath:
                //   //     'https://corporatephotographymelbourne.com.au/wp-content/uploads/2019/05/Corporate-suit-business-man-lawyer-law-photography-melbourne-5-1.jpg',
                //   isCircle: true,
                //   height: 50,
                //   width: 50,
                //   boxFit: BoxFit.cover,
                // ),
                rate.userId!.image == null
                    ? Container(
                        height: 40,
                        width: 40,
                        decoration: const BoxDecoration(
                          color: Colors.grey,
                          shape: BoxShape.circle,
                        ),
                        child: const Icon(
                          Icons.person_outline,
                          color: Colors.white,
                        ),
                      )
                    : MainImage(
                        imageType: ImageType.network,
                        imagePath: rate.userId!.image!,
                        isCircle: true,
                        height: 40,
                        width: 40,
                        boxFit: BoxFit.cover,
                      ),
                const SizedBox(width: 10),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "${rate.userId!.firstName}",
                      // 'Khaled ahmed',
                      style: const TextStyle(
                        fontWeight: FontWeight.w900,
                        fontSize: 16,
                      ),
                    ),
                    const Text(
                      "seo iqlife",
                      style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: 12,
                      ),
                    ),
                    Wrap(
                      children: [
                        StarsRatingWidget(
                          spacing: 0,
                          rating: double.parse(rate.value ?? "0"),
                          color: Colors.yellow.shade700,
                          iconSize: 10,
                        ),
                        const SizedBox(width: 10),
                        Text(
                          "(${rate.value}/5)",
                          style: const TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 10,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(height: 10),
            Text(
              "${rate.message}",
              style: const TextStyle(
                fontSize: 13,
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
      ),
      onTap: () {
        // DialogHelper.hireDialog(context);
      },
    );
  }
}
