
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassity/core/models/project.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/pages/offers_stage_page.dart';
import 'package:wassity/view/styles/app_colors.dart';
import 'package:wassity/view/view_helper/app_helper.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/widgets/components/main_image.dart';

class MyProjectsItem extends StatelessWidget {
  final Project project ;

  const MyProjectsItem({Key? key , required this.project}) : super(key: key);


  @override
  Widget build(BuildContext context) {
  final AppLocalizations locale = AppLocalizations.of(context);
    return InkWell(
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 7),
        height: SizeConfig.height * 0.15,
        width: double.infinity,
        child: Row(
          children: [
            MainImage(
              height: SizeConfig.height * 0.15,
              width: SizeConfig.height * 0.15,
              imageType: ImageType.network,
              radius: 5,
              boxFit: BoxFit.cover,
              imagePath:
                  "${project.image}",
            ),
            const SizedBox(width: 10),
            Column(
              mainAxisAlignment:
                  MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                 Hero(
                   tag: "${project.id}",
                   child: Text(
                   "${project.title}",
                    style: const TextStyle(
                      fontWeight: FontWeight.w900,
                      fontSize: 16,
                    ),
                                 ),
                 ),
                Wrap(
                  alignment: WrapAlignment.spaceBetween,
                  // crossAxisAlignment: CrossAxisAlignment.,
                  children: [
                    Wrap(
                      children:  [
                        const Icon(
                          Icons.person_outlined,
                          size: 15,
                        ),
                        Text(
                          "${project.user!.firstName} ${project.user!.lastName}",
                          style:const  TextStyle(
                            color: Colors.black45,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(width: 10),
                    Wrap(
                      children:  [
                      const  Icon(
                          Icons.access_time,
                          size: 15,
                        ),
                        Text(
                          project.user?.updateDates?.updatedAtHuman?? "",
                          overflow: TextOverflow.ellipsis,
                          style:const TextStyle(
                            color: Colors.black45,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Container(
                  padding: const EdgeInsets.symmetric(
                      vertical: 5, horizontal: 10),
                  decoration: BoxDecoration(
                    color:
                        AppColors.primaryColor.withAlpha(20),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      Text(
                        locale.get("50"),
                        style: const TextStyle(
                          color: AppColors.primaryColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SvgPicture.asset(
                        "assets/svgs/save-money.svg",
                        color: AppColors.primaryColor,
                        height: 17,
                        width: 17,
                      ),
                      // SizedBox(width: 10),
                      Text(
                        locale.get("Offer"),
                        style: const TextStyle(
                          color: AppColors.primaryColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      onTap: (){
        AppHelper.push(context,  OfferStagePage(project: project));
      },
    );
  }
}