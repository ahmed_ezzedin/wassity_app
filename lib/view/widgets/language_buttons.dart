
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/styles/app_colors.dart';

class LanguageButtons extends StatelessWidget {
  const LanguageButtons({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    final appLanguage = Provider.of<AppLanguage>(context);
    bool isEn = appLanguage.appLocale.languageCode == "en";
    return Padding(
      padding: const EdgeInsets.only(top: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          InkWell(
            child: Text(
              locale.get("English"),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: isEn ? AppColors.primaryColor : null,
                // fontSize: 10,
                decoration: isEn ? TextDecoration.underline : null,
                decorationColor: AppColors.primaryColor,
                decorationThickness: 3,
              ),
            ),
            onTap: () {
              appLanguage.changeLanguage(const Locale("en"));
            },
          ),
          const SizedBox(width: 20),
          InkWell(
            child: Text(
              locale.get("عربي"),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: !isEn ? AppColors.primaryColor : null,
                // fontSize: 10,
                decoration: !isEn ? TextDecoration.underline : null,
                decorationColor: AppColors.primaryColor,
                decorationThickness: 3,
              ),
            ),
            onTap: () {
              appLanguage.changeLanguage(const Locale("ar"));
            },
          ),
        ],
      ),
    );
  }
}
