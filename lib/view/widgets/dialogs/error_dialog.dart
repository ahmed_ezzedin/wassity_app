import 'package:flutter/material.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/view_helper/size_config.dart';

class ErrorDialog extends StatelessWidget {
  final String? message;
  final bool isSuccess;

  const ErrorDialog({
    Key? key,
    this.isSuccess = false,
    this.message,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return Dialog(
      // elevation: 0,
      backgroundColor: Colors.transparent,
      insetPadding: const EdgeInsets.all(0),
      child: Stack(
        alignment: Alignment.topCenter,
        children: [
          // ========================================================= Dialog Content
          Container(
            height: SizeConfig.height * 0.2,
            width: SizeConfig.width * 0.85,
            margin: const EdgeInsets.only(top: 40),
            decoration: BoxDecoration(
              color: Theme.of(context).dialogBackgroundColor,
              borderRadius: BorderRadius.circular(15),
            ),
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                children: [
                  const SizedBox(height: 60),
                  Text(
                    isSuccess
                        ? message ?? locale.get("Congratulations you won.")
                        : message ?? locale.get("Something went wrong please try again later !"),
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ),
          ),

          // ========================================================= Dialog Icon
          Container(
            padding: const EdgeInsets.all(15),
            decoration: BoxDecoration(
              color: isSuccess ? Colors.greenAccent : Colors.red,
              border: Border.all(
                width: 3,
                color: Theme.of(context).dialogBackgroundColor,
              ),
              shape: BoxShape.circle,
            ),
            child: isSuccess
                ? const Icon(
                    Icons.check,
                    size: 40,
                    color: Colors.white,
                  )
                : const Icon(
                    Icons.warning,
                    size: 40,
                    color: Colors.white,
                  ),
          ),
        ],
      ),
    );
  }
}
