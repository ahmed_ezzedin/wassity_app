// ignore_for_file: prefer_const_constructors_in_immutables

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/styles/app_colors.dart';

class LanguageDialog extends StatelessWidget {
  final BuildContext context;
  // ignore: use_key_in_widget_constructors
  LanguageDialog({required this.context});

  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    final appLanguage = Provider.of<AppLanguage>(context);
    List<String> themeModes = [
      "English",
      "العَرَبِيَّة",
    ];
    int themeGroupValue = themeModes.indexOf(appLanguage.languageName);
    return StatefulBuilder(
      builder: (context, builderSetstate) {
        return Dialog(
          insetPadding: const EdgeInsets.all(15),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          child: Container(
            padding: const EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  locale.get("Language"),
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                ),
             const    SizedBox(height: 10),
                Column(
                  children: themeModes
                      .map(
                        (e) => RadioListTile<int>(
                          contentPadding: const EdgeInsets.all(0),
                          title: Text(
                            locale.get(e),
                            style:const  TextStyle(
                              fontSize: 16,
                            ),
                          ),
                          value: themeModes.indexWhere((element) => element == e),
                          groupValue: themeGroupValue,
                          onChanged: (value) {
                            themeGroupValue = value!;
                            builderSetstate(() {});
                          },
                        ),
                      )
                      .toList(),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TextButton(
                      child: Text(
                        locale.get("Cancel"),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    ElevatedButton(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                        child: Text(
                          locale.get("Done"),
                          style: const TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                        primary: AppColors.primaryColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop(themeModes[themeGroupValue]);
                      },
                    )
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}