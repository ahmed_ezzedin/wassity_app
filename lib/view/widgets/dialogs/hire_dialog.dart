import 'package:flutter/material.dart';
import 'package:wassity/core/models/mediator.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/pages/nav_bar/home_tab.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/widgets/components/main_image.dart';
import 'package:wassity/view/widgets/components/stars_rating_widget.dart';

class HireDialog extends StatelessWidget {
  final BuildContext context;
  final HomeTabModel model;
  final Mediator mediator;

  const HireDialog({Key? key, required this.context ,required this.model, required this.mediator}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      insetPadding: const EdgeInsets.all(15),
      content: SizedBox(
        height: SizeConfig.height * 0.7,
        child: Column(
          // mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Align(
              alignment: Alignment.topRight,
              child: IconButton(
                icon: Icon(
                  Icons.close,
                  color: Colors.grey.shade400,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
            const SizedBox(),
            const SizedBox(),
            const SizedBox(),
            const SizedBox(),
            mediator.image == null
                ? Container(
                    height: 40,
                    width: 40,
                    decoration: const BoxDecoration(
                      color: Colors.grey,
                      shape: BoxShape.circle,
                    ),
                    child: const Icon(
                      Icons.person_outline,
                      color: Colors.white,
                    ),
                  )
                : MainImage(
                    imageType: ImageType.network,
                    imagePath: "${mediator.image}",
                    isCircle: true,
                    height: 80,
                    width: 80,
                    boxFit: BoxFit.cover,
                  ),
            Text(
              '${mediator.firstName} ${mediator.lastName}',
              style: const TextStyle(
                fontWeight: FontWeight.w900,
                fontSize: 26,
              ),
            ),
            const Text(
              "CEO IQ2LIFE",
              style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 14,
              ),
            ),
            Wrap(
              children: [
                 StarsRatingWidget(
                  spacing: 0,
                  rating: double.parse(mediator.rate?? "0"),
                  color: Colors.orange,
                  iconSize: 15,
                ),
                const SizedBox(width: 10),
                Text(
                  "(${mediator.rate}/5)",
                  style: const TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 12,
                  ),
                ),
              ],
            ),
            SizedBox(height: SizeConfig.height * 0.01),
            Text(
              locale.get('Do you want to accept ${mediator.firstName} \'s offer?'),
              style: const TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
            SizedBox(height: SizeConfig.height * 0.008),
            Text(
              locale.get(
                  'The site will be a mediator between you and ${mediator.firstName} until the transaction is completed'),
              textAlign: TextAlign.center,
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w500,
              ),
            ),
            const SizedBox(),
            const SizedBox(),
            const SizedBox(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ElevatedButton(
                  child: Text(
                    locale.get("Hire Him"),
                    style: const TextStyle(fontSize: 16),
                  ),
                  onPressed: () {
                    // Navigator.of(context).pop();
                    // AppHelper.push(context, OfferStagePage());
                    model.hire(
                      userId: mediator.id!,
                      projectId: 5,
                    );
                  },
                ),
                ElevatedButton(
                  child: Text(
                    locale.get("Cancel"),
                    style: const TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                    primary: Colors.black54,
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
            const SizedBox(),
          ],
        ),
      ),
    );
  }
}
