import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/view_helper/size_config.dart';

class CongratulationDialog extends StatelessWidget {
  final BuildContext context;


  const CongratulationDialog({Key? key ,     required this.context,}) : super(key: key);
 

  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      insetPadding: const EdgeInsets.all(20),
      content: SizedBox(
        height: SizeConfig.height * 0.7,
        child: Column(
          // mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            const SizedBox(),
            const SizedBox(),
            const SizedBox(),
            const SizedBox(),
            SvgPicture.asset("assets/svgs/tick.svg"),
            Text(
              locale.get('Congratulations'),
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w500,
              ),
            ),
            const SizedBox(),
            Text(
              locale.get(
                  'Your Deal Has Been Successfully Added.. Please Wait For The Brokers To Place Their Offers On Your Deal'),
              textAlign: TextAlign.center,
              style: const TextStyle(
                fontSize: 13,
                fontWeight: FontWeight.w400,
              ),
            ),
            const SizedBox(),
            const SizedBox(),
            const SizedBox(),
            ElevatedButton(
              child: Text(
                locale.get("Add A New Deal"),
                style: const TextStyle(fontSize: 16),
              ),
              onPressed: () {
                Navigator.of(context).pop();
                // AppHelper.push(context, const OfferStagePage());
              },
            ),
          ],
        ),
      ),
    );
  }
}
