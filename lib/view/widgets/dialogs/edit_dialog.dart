import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:wassity/core/models/user_model.dart';
import 'package:wassity/core/services/core_helper/file_helper.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/core/services/provider/provider_setup.dart';
import 'package:wassity/view/view_helper/app_helper.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/widgets/components/main_image.dart';
import 'package:wassity/view/widgets/components/main_progress.dart';

class EditDialog extends StatefulWidget {
  final BuildContext context;


 const  EditDialog({
    Key? key,
    required this.context}) : super(key: key);

  @override
  State<EditDialog> createState() => _EditDialogState();
}

class _EditDialogState extends State<EditDialog> {
  TextEditingController bioController = TextEditingController();

  TextEditingController mobileController = TextEditingController();

  TextEditingController firstNameController = TextEditingController();

  TextEditingController lastNameController = TextEditingController();

  bool isLoading = false;

  void updateProfile() async {
    isLoading = true;
    setState(() {});

    final res = await api.updateProfile(
      widget.context,
      body: {
        "first_name": firstNameController.text,
        "last_name": lastNameController.text,
        "phone": mobileController.text,
        "bio": bioController.text,
      },
    );
    if (res != null) {
      if (res["success"] == true) {
        if (res["data"] != null) {
          UserModel? tempUser = auth.userModel;
          tempUser!.user!.firstName = res["data"]["first_name"];
          tempUser.user!.lastName = res["data"]["last_name"];
          tempUser.user!.phone = res["data"]["phone"];
          tempUser.user!.bio = res["data"]["bio"];
          auth.saveUser(tempUser);
        }
      } else {
        if (res["message"] != null) {
          AppHelper.showSnackBarMessage(widget.context,
              message: "${res["message"]}");
        }
      }
    }
    isLoading = false;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    bioController.text = auth.userModel?.user?.bio ?? "";
    mobileController.text = auth.userModel?.user?.email ?? "";
    firstNameController.text = auth.userModel?.user?.firstName ?? "";
    lastNameController.text = auth.userModel?.user?.lastName ?? "";

    final locale = AppLocalizations.of(context);
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      insetPadding: const EdgeInsets.all(10),
      content: Container(
        height: SizeConfig.height * 0.7,
        width: double.infinity,
        color: Colors.white,
        child: Column(
          // mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Align(
              alignment: Alignment.topRight,
              child: IconButton(
                icon: Icon(
                  Icons.close,
                  color: Colors.grey.shade400,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
            InkWell(
              child: Stack(
                children: [
                  const MainImage(
                    imageType: ImageType.network,
                    imagePath:
                        'https://corporatephotographymelbourne.com.au/wp-content/uploads/2019/05/Corporate-suit-business-man-lawyer-law-photography-melbourne-5-1.jpg',
                    isCircle: true,
                    height: 100,
                    width: 100,
                    boxFit: BoxFit.cover,
                  ),
                  Container(
                    height: 100,
                    width: 100,
                    decoration: const BoxDecoration(
                      color: Colors.black45,
                      shape: BoxShape.circle,
                    ),
                    child: const Icon(
                      Icons.camera_alt_outlined,
                      color: Colors.white,
                      size: 40,
                    ),
                  ),
                ],
              ),
              onTap: () async {
                final result = await FileHelper.pickImage(ImageSource.gallery);
                if (result != null) {
                  // ignore: avoid_print
                  print("result");
                }
              },
            ),
            const SizedBox(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  flex: 3,
                  child: TextField(
                    controller: mobileController,
                    decoration: const InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      suffix: Icon(
                        Icons.edit_outlined,
                        size: 22,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: TextField(
                    controller: firstNameController,
                    decoration: const InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      suffix: Icon(
                        Icons.edit_outlined,
                        size: 22,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: TextField(
                    controller: lastNameController,
                    decoration: const InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      suffix: Icon(
                        Icons.edit_outlined,
                        size: 22,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(),
            const SizedBox(),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      locale.get("Brief About Me"),
                      style: const TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    const SizedBox(),
                  ],
                ),
                TextField(
                  controller: bioController,
                  textInputAction: TextInputAction.newline,
                  maxLines: 5,
                  decoration: const InputDecoration(
                    border: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    hintMaxLines: 3,
                    suffix: Icon(
                      Icons.edit_outlined,
                      size: 22,
                      color: Colors.grey,
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(),
            const SizedBox(),
            isLoading
                ? const MainProgress()
                : ElevatedButton(
                    child: Text(locale.get("Edit")),
                    onPressed: () {
                      updateProfile();
                    },
                  )
          ],
        ),
      ),
    );
  }
}
