import 'package:flutter/material.dart';
import 'dart:io';

import 'package:wassity/view/styles/app_colors.dart';
import 'package:wassity/view/widgets/components/main_progress.dart';

enum ImageType { network, asset, file }

class MainImage extends StatelessWidget {
  final ImageType imageType;
  final String imagePath;
  final BoxFit boxFit;
  final double height;
  final double width;
  final double? latitude;
  final double? longitude;
  final double radius;
  final bool isCircle;

  const MainImage({
    Key? key,
    required this.imageType,
    required this.imagePath,
    this.boxFit = BoxFit.contain,
    this.height = 100,
    this.width = 100,
    this.latitude,
    this.longitude,
    this.radius = 0,
    this.isCircle = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    switch (imageType) {

      ///===================================================== [ Network ]
      case ImageType.network:
        return SizedBox(
          height: height,
          width: width,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(isCircle ? height / 2 : radius),
            child: Image.network(
              imagePath,
              fit: boxFit,
              errorBuilder: (_, obj, trace) => const Icon(
                Icons.broken_image,
                size: 50,
                color: AppColors.primaryColor,
              ),
              loadingBuilder: (
                BuildContext context,
                Widget child,
                ImageChunkEvent? loadingProgress,
              ) {
                if (loadingProgress == null) {
                  return child;
                } else {
                  return const Center(
                    child: MainProgress(
                      color: AppColors.primaryColor,
                      stroke: 2,
                    ),
                  );
                }
              },
            ),
          ),
        );

      ///===================================================== [ Asset ]
      case ImageType.asset:
        return SizedBox(
          height: height,
          width: width,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(isCircle ? height / 2 : radius),
            child: Image.asset(
              imagePath,
              fit: boxFit,
              errorBuilder: (_, obj, trace) => const Icon(
                Icons.broken_image,
                size: 50,
                color: AppColors.primaryColor,
              ),
            ),
          ),
        );

      ///===================================================== [ File ]
      case ImageType.file:
        return SizedBox(
          height: height,
          width: width,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(isCircle ? height / 2 : radius),
            child: Image.file(
              File(imagePath),
              fit: boxFit,
              errorBuilder: (_, obj, trace) => const Icon(
                Icons.broken_image,
                size: 50,
                color: AppColors.primaryColor,
              ),
            ),
          ),
        );

      ///===================================================== [ default ]
      default:
        return const Text("default!");
    }
  }
}
