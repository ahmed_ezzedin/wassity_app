import 'package:flutter/material.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/styles/app_colors.dart';

enum ProgressType { circular, linear }

class MainProgress extends StatelessWidget {
  const MainProgress({Key? key ,   this.color = AppColors.primaryColor,
    this.stroke = 4,
    this.diameter = 35,
    this.height = 4,
    this.linearWidth,
    this.type = ProgressType.circular,}) : super(key: key);

  final Color color;
  final double diameter;
  final double stroke;
  final ProgressType type;
  final double height;
  final double? linearWidth;

  @override
  Widget build(BuildContext context) {
    Widget progress;
    switch (type) {
      case ProgressType.circular:
        progress = Center(
          child: SizedBox(
            height: diameter,
            width: diameter,
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(color),
              strokeWidth: stroke,
            ),
          ),
        );
        break;
      case ProgressType.linear:
        progress = SizedBox(
          width: SizeConfig.width,
          child: LinearProgressIndicator(
            minHeight: height,
            valueColor: AlwaysStoppedAnimation<Color>(color),
          ),
        );
        break;
    }
    return progress;
  }
}
