// import 'package:flutter/material.dart';

// class MainAppBar extends StatefulWidget implements PreferredSizeWidget {
//   final String? title;
//   final List<Widget>? actions;
//   final leading;

//   MainAppBar({
//     this.title,
//     this.actions,
//     this.leading,
//   }) : preferredSize = Size.fromHeight(kToolbarHeight);
//   @override
//   final Size preferredSize;
//   @override
//   _MainAppBarState createState() => _MainAppBarState();
// }

// class _MainAppBarState extends State<MainAppBar> {
//   @override
//   Widget build(BuildContext context) {
//     return AppBar(
//       leading: widget.leading,
//       title: Text(
//         "${widget.title}",
//         style: Theme.of(context).appBarTheme.titleTextStyle,
//       ),
//       actions: widget.actions,
//     );
//   }
// }
