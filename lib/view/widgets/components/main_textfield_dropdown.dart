import 'package:flutter/material.dart';
import 'package:wassity/view/styles/app_colors.dart';

class MainTextFieldDropdown extends StatelessWidget {
  final List<dynamic> items;
  final dynamic initialValue;
  final String? hint;
  final String? label;
  final bool isFilled;
  final Color? fillColor;
  final EdgeInsetsGeometry? contentPadding;
  final Function(dynamic)? onChanged;

  const MainTextFieldDropdown({
    Key? key,
    required this.items,
    this.initialValue,
    this.hint,
    this.label,
    this.isFilled = false,
    this.fillColor,
    this.contentPadding,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: DropdownButtonFormField(
        items: items.map((item) {
          return DropdownMenuItem(
            value: item,
            child: Text(item),
          );
        }).toList(),
        value: initialValue,
        onChanged: onChanged,
        icon:const  Icon(Icons.keyboard_arrow_down),
        decoration: InputDecoration(
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
          filled: isFilled,
          fillColor: fillColor,
          hintText: hint,
          labelText: label,
          hintStyle: const  TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
          labelStyle:const  TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
          border:
             const  UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
          enabledBorder:
            const   UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
          focusedBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: AppColors.primaryColor)),
          // enabledBorder: OutlineInputBorder(
          //   borderRadius: BorderRadius.circular(10),
          //   borderSide: BorderSide(color: Colors.grey, width: 1.2),
          // ),
          // focusedBorder: OutlineInputBorder(
          //   borderSide: BorderSide(color: AppColors.primaryColor, width: 1.2),
          //   borderRadius: BorderRadius.circular(10),
          // ),
          // // in case text field has error and focused in the same time
          // border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          // disabledBorder: OutlineInputBorder(
          //   borderRadius: BorderRadius.circular(10),
          //   borderSide: BorderSide(color: Colors.grey, width: 1.2),
          // ),
          // focusedErrorBorder: OutlineInputBorder(
          //   borderRadius: BorderRadius.circular(10),
          //   borderSide: BorderSide(color: Colors.red, width: 1.2),
          // ),
        ),
        validator: (value) {
          return value != null ? null : "$label is Reqired!";
        },
      ),
    );
  }
}
