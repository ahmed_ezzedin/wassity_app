import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/view_helper/size_config.dart';

class LoginWithButtons extends StatelessWidget {
  const LoginWithButtons({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final AppLocalizations locale = AppLocalizations.of(context);
    return Column(
      children: [
        SizedBox(height: SizeConfig.height * 0.05),
        Center(
          child: Wrap(
            crossAxisAlignment: WrapCrossAlignment.center,
            children: [
              Container(
                height: 2,
                width: SizeConfig.width * 0.2,
                color: Colors.grey,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Text(
                  locale.get("Or"),
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                height: 2,
                width: SizeConfig.width * 0.2,
                color: Colors.grey,
              ),
            ],
          ),
        ),
        SizedBox(height: SizeConfig.height * 0.01),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            const SizedBox(),
            IconButton(
              icon: SvgPicture.asset(
                "assets/svgs/google.svg",
                color: Colors.red,
              ),
              onPressed: () {},
            ),
            IconButton(
              icon: SvgPicture.asset(
                "assets/svgs/facebook.svg",
                color: Colors.blue.shade700,
              ),
              onPressed: () {},
            ),
            IconButton(
              icon: SvgPicture.asset(
                "assets/svgs/twitter.svg",
                color: Colors.blue.shade300,
            ),
              onPressed: () {},
            ),
            const SizedBox(),
          ],
        ),
        SizedBox(height: SizeConfig.height * 0.05),
      ],
    );
  }
}
