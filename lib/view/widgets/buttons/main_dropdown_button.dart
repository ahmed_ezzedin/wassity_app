import 'package:flutter/material.dart';

class MainDropdownButton extends StatelessWidget {
  final String value;
  final List<dynamic> items;
  final Function(dynamic) onChanged;

  const MainDropdownButton({Key? key  ,     required this.value,
    required this.items,
    required this.onChanged,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10 , vertical: 5),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey  ,width: 0.5),
        borderRadius: BorderRadius.circular(3),
      ),
      child: DropdownButton<dynamic>(
        isDense: true,
        value: value,
        items: items.map<DropdownMenuItem<dynamic>>((dynamic value) {
          return DropdownMenuItem<dynamic>(
            value: value,
            child: Text(value),
          );
        }).toList(),
        style: const TextStyle(
          color: Colors.black54,
          fontWeight: FontWeight.bold,
          fontSize: 12,
        ),
        underline:const SizedBox(),
        onChanged: onChanged,
      ),
    );
  }
}
