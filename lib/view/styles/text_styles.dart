import 'package:flutter/material.dart';
import 'package:wassity/view/styles/app_colors.dart';

class TextStyles {
  static const headerStyle = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.bold,
    color: AppColors.accentColor,
  );
  static const supHeaderStyle = TextStyle(fontSize: 16, color: AppColors.text2);

  static const heading1Style = TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 15,
  );
}
