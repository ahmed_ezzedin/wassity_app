// Class that will hold most of the reusable UI components and operators like Push and time picker

import 'dart:convert';
import 'dart:io';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:wassity/view/styles/app_colors.dart';

class AppHelper {
//================================================================= Push
  static Future<dynamic> push(BuildContext context, Widget page,
      {bool replace = false}) async {
    final route = MaterialPageRoute(builder: (ctx) => page);
    return replace
        ? await Navigator.pushReplacement(context, route)
        : await Navigator.push(context, route);
  }

//================================================================= Push And Replace
  static Future<dynamic> pushReplaceAll(BuildContext context, Widget page) {
    return Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => page),
        (Route<dynamic> route) => false);
  }

//================================================================= Unfocus
  static unfocusFun(BuildContext context) {
    FocusScope.of(context).unfocus();
  }

//================================================================= Normal Print

static appPrint(Object? text){
  // ignore: avoid_print
  print(text);
}

//================================================================= Colored Print
  static printText(dynamic text, {String color = "green"}) {
    Map<String, String> colorsMap = {
      // ==================>> Text colors
      "reset": "\x1B[0m",
      "black": "\x1B[30m",
      "red": "\x1B[31m",
      "green": "\x1B[32m",
      "yellow": "\x1B[33m",
      "blue": "\x1B[34m",
      "magenta": "\x1B[35m",
      "cyan": "\x1B[36m",
      "white": "\x1B[37m",
      // ==================>>  Background colors
      "back1": "\x1B[40m",
      "back2": "\x1B[41m",
      "back3": "\x1B[42m",
      "back4": "\x1B[43m",
      "back5": "\x1B[44m",
      "back6": "\x1B[45m",
      "back7": "\x1B[46m",
      "back8": "\x1B[47m",
    };
    // ignore: avoid_print
    print("${colorsMap[color] ?? "\x1B[32m"} $text\x1B[0m");
  }

//=================================================================  Print Formated Object
  static printObject(Object? object) {
    // ignore: avoid_print
    print("\x1B[35m ************************************************* \x1B[0m");
    // print("${JsonEncoder.withIndent("     ").convert(object)}");
    /// =================> Spaces is for space between opject items
    const JsonEncoder.withIndent("     ").convert(object).split("\n").forEach(
      (element) {
         // ignore: avoid_print
         print("\x1B[35m$element\x1B[0m");
      },
    );
    // ignore: avoid_print
    print("\x1B[35m ************************************************* \x1B[0m");
  }

//================================================================= Convert Object To String
  static String objectToString(Object? object) {
    return const JsonEncoder.withIndent("   ").convert(object);
  }

//================================================================= Toast
  static showToastMesssage(
    String message, {
    double fontSize = 15,
    textColor = Colors.white,
    gravity = ToastGravity.BOTTOM,
    toastLength = Toast.LENGTH_SHORT,
  }) {
    Fluttertoast.cancel();
    Fluttertoast.showToast(
      msg: message,
      fontSize: fontSize,
      textColor: textColor,
      gravity: gravity,
      toastLength: toastLength,
      backgroundColor: Colors.grey.shade700,
    );
  }

//================================================================= Snack Bar
  static showSnackBarMessage(
    BuildContext context, {
    required String message,
    String? label,
    Duration duration = const Duration(seconds: 4),
    SnackBarBehavior? behavior,
    double radius = 0,
  }) {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
        duration: duration,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(radius)),
        behavior: behavior,
        action: label == null
            ? null
            : SnackBarAction(
                label: label,
                textColor: AppColors.primaryColor,
                onPressed: () {
                  Navigator.of(context);
                },
              ),
      ),
    );
  }

//================================================================= Check Internet Connection
  static Future<bool> hasInternet(BuildContext context) async {
    bool hasInternet = false;
    try {
      final result = await InternetAddress.lookup("google.com");
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        hasInternet = true;
      }
    } on SocketException catch (e) {
      // ignore: avoid_print
      print("$e");
      hasInternet = false;
    }
    return hasInternet;
  }

//================================================================= Formate Time
static String formatDateTime(DateTime dateTime , {String pattern = "h:mm a EEEE d MMM y"}){
// 'EEE, MMM d, ''yy'             Wed, Jul 10, '96
// 'yyyyy.MMMMM.dd GGG hh:mm a'   01996.July.10 AD 12:08 PM
  return DateFormat(pattern).format(dateTime);
}

//================================================================= Time Picker
  static Future<TimeOfDay?> getTimePicker(
      BuildContext context, TimeOfDay initialTime,
      {Color color = AppColors.primaryColor}) async {
    TimeOfDay? timeOfDay = await showTimePicker(
      context: context,
      initialTime: initialTime,
      builder: (BuildContext context ,Widget?  child){
        return Theme(data: Theme.of(context).copyWith(
          primaryColor: Theme.of(context).primaryColor,
          backgroundColor: Theme.of(context).dialogBackgroundColor,
        ), child: child!,)
      }
    );
    return timeOfDay;
  }

//================================================================= Date Picker
  static Future<DateTime?> getDatePicker(BuildContext context, DateTime initialDate,
      {Color color = AppColors.primaryColor,}) async {
    DateTime? dateTime = await showDatePicker(
      context: context,
      initialDate: initialDate,
      firstDate: DateTime(1900),
      lastDate: DateTime.now(),
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: Theme.of(context).primaryColor,
            colorScheme: ColorScheme.light(primary: color),
          ),
          child: child!,
        );
      },
    );
    return dateTime;
  }

//================================================================= Date Range Picker
  static Future<DateTimeRange?> getDateRangePicker(
    BuildContext context, {
    required DateTime firstDate,
    required DateTime lastDate,
    Color color = AppColors.primaryColor,
  }) async {
    DateTimeRange? dateTimeRange = await showDateRangePicker(
      context: context,
      firstDate: firstDate,
      lastDate: lastDate,
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: color,
          ),
          child: child!,
        );
      },
    );
    return dateTimeRange;
  }

}
