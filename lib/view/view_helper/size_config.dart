import 'package:flutter/widgets.dart';

class SizeConfig {
  static late double height;
  static late double width;
  static late double textSize;
  static late double imageSize;
  static bool isPortrait = true;
  // static bool isMobilePortrait = false;

  void initialize(BoxConstraints constraints, Orientation orientation) {
    if (orientation == Orientation.portrait) {
      height = constraints.maxHeight;
      width = constraints.maxWidth;
      isPortrait = true;
      // if (width< 450) {
      //   isPortrait = true;
      // }
    } else {
      // height = constraints.maxHeight;
      height = constraints.maxWidth;
      width = constraints.maxWidth;
      // width = constraints.maxHeight + constraints.maxHeight * 0.5;
      // height = constraints.maxWidth;
      isPortrait = false;
      // isMobilePortrait = false;
    }
    textSize = height * 0.5;
    imageSize = width;
  }
}
