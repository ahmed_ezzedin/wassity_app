// ignore_for_file: prefer_const_constructors, file_names

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/core/services/provider/provider_setup.dart';
import 'package:wassity/view/pages/auth/password_changed_page.dart';
import 'package:wassity/view/styles/app_colors.dart';
import 'package:wassity/view/styles/text_styles.dart';
import 'package:wassity/view/view_helper/app_helper.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/widgets/buttons/main_button.dart';
import 'package:wassity/view/widgets/components/main_progress.dart';
import 'package:wassity/view/widgets/components/main_textfield.dart';
import 'package:wassity/view/widgets/language_buttons.dart';

class ChangePasswordPage extends StatelessWidget {
  const ChangePasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return BaseWidget<ChangePasswordPageModel>(
      model: ChangePasswordPageModel(context: context),
      builder: (_, model, child) {
        return Scaffold(
          body: SafeArea(
            child: model.busy
                ? MainProgress()
                : Container(
                    padding: const EdgeInsets.all(15),
                    width: double.infinity,
                    child: SingleChildScrollView(
                      child: Form(
                        key: model.formKey,
                        autovalidateMode: model.autovalidateMode,
                        child: Stack(
                          alignment: Alignment.topLeft,
                          children: [
                            Positioned(
                              top: -100,
                              left: -100,
                              child:
                                  SvgPicture.asset("assets/svgs/Group 290.svg"),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Center(
                                  child: Column(
                                    children: [
                                      LanguageButtons(),
                                      SizedBox(
                                          height: SizeConfig.height * 0.07),
                                      Image.asset(
                                        "assets/images/app_logo.png",
                                        height: SizeConfig.width * 0.25,
                                        width: SizeConfig.width * 0.25,
                                      ),
                                      SizedBox(
                                          height: SizeConfig.height * 0.03),
                                      Text(
                                        locale.get("CHANGE PASSWORD"),
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                        ),
                                      ),
                                      SizedBox(
                                          height: SizeConfig.height * 0.015),
                                      RichText(
                                        text: TextSpan(
                                          style: TextStyle(
                                            fontSize: 13,
                                          ),
                                          children: <TextSpan>[
                                            TextSpan(
                                                text: locale.get(
                                                    "Change Your Password For ")),
                                            TextSpan(
                                              text: locale.get(' Waseati '),
                                              style: TextStyle(
                                                fontWeight: FontWeight.w900,
                                                color: AppColors.primaryColor,
                                              ),
                                            ),
                                            TextSpan(
                                                text: locale.get("Platform")),
                                          ],
                                        ),
                                        textScaleFactor: 1.0,
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(height: SizeConfig.height * 0.10),
                                Text(
                                  locale.get("Old Password"),
                                  style: TextStyles.heading1Style,
                                ),
                                SizedBox(height: SizeConfig.height * 0.005),
                                MainTextField(
                                  controller: model.oldPasswordController,
                                  hint: "****************",
                                  suffixIcon: Padding(
                                    padding: const EdgeInsets.all(15),
                                    child: SvgPicture.asset(
                                        "assets/svgs/lock (1).svg"),
                                  ),
                                  validator: Validator.password,
                                ),
                                SizedBox(height: SizeConfig.height * 0.01),
                                Text(
                                  locale.get("New Password"),
                                  style: TextStyles.heading1Style,
                                ),
                                SizedBox(height: SizeConfig.height * 0.005),
                                MainTextField(
                                  controller: model.passwordController,
                                  hint: "****************",
                                  suffixIcon: Padding(
                                    padding: const EdgeInsets.all(15),
                                    child: SvgPicture.asset(
                                        "assets/svgs/lock (1).svg"),
                                  ),
                                  validator: Validator.password,
                                ),
                                SizedBox(height: SizeConfig.height * 0.01),
                                Text(
                                  locale.get("Confirm New Password"),
                                  style: TextStyles.heading1Style,
                                ),
                                SizedBox(height: SizeConfig.height * 0.005),
                                MainTextField(
                                  controller: model.confirmPasswordController,
                                  hint: "****************",
                                  suffixIcon: Padding(
                                    padding: const EdgeInsets.all(15),
                                    child: SvgPicture.asset(
                                        "assets/svgs/lock (1).svg"),
                                  ),
                                  validator: Validator.isMatch,
                                  matchedValue: model.passwordController.text,
                                ),
                                SizedBox(height: SizeConfig.height * 0.10),
                                MainButton(
                                  title: locale.get("Send"),
                                  onPressed: () {
                                    model.submitFun();
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
          ),
        );
      },
    );
  }
}

class ChangePasswordPageModel extends BaseModel {
  final BuildContext context;

  ChangePasswordPageModel({required this.context});
  final formKey = GlobalKey<FormState>();
  var autovalidateMode = AutovalidateMode.disabled;
  final oldPasswordController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();

  void submitFun() {
    if (formKey.currentState!.validate()) {
      changePasswordFun();
    } else {
      autovalidateMode = AutovalidateMode.always;
      setState();
    }
  }

  void changePasswordFun() async {
    setBusy();
    final result = await auth.changePassword(
      context,
      body: {
        "oldpassword": oldPasswordController.text,
        "password": passwordController.text,
        "password_confirmation": confirmPasswordController.text,
      },
    );
    if (result) {
      AppHelper.push(context, PasswordChangedPage());
    }
    setIdle();
  }
}
