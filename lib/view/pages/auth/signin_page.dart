// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/core/services/provider/provider_setup.dart';
import 'package:wassity/view/pages/auth/forgot_password_page.dart';
import 'package:wassity/view/pages/auth/signup_page.dart';
import 'package:wassity/view/pages/nav_bar/home_nav_page.dart';
import 'package:wassity/view/styles/app_colors.dart';
import 'package:wassity/view/styles/text_styles.dart';
import 'package:wassity/view/view_helper/app_helper.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/widgets/buttons/main_button.dart';
import 'package:wassity/view/widgets/components/main_progress.dart';
import 'package:wassity/view/widgets/components/main_textfield.dart';
import 'package:wassity/view/widgets/language_buttons.dart';
import 'package:wassity/view/widgets/login_with_buttons.dart';

class SigninPage extends StatelessWidget {
  const SigninPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);

    return BaseWidget<SigninPageModel>(
      model: SigninPageModel(context: context),
      builder: (_, model, child) {
        return Scaffold(
          body: SafeArea(
            child: model.busy
                ? MainProgress()
                : Container(
                    padding: const EdgeInsets.all(15),
                    width: double.infinity,
                    child: SingleChildScrollView(
                      child: Form(
                        key: model.formKey,
                        autovalidateMode: model.autovalidateMode,
                        child: Stack(
                          alignment: Alignment.topLeft,
                          children: [
                            Positioned(
                              top: -100,
                              left: -100,
                              child:
                                  SvgPicture.asset("assets/svgs/Group 290.svg"),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Center(
                                  child: Column(
                                    children: [
                                      LanguageButtons(),
                                      SizedBox(
                                          height: SizeConfig.height * 0.07),
                                      Image.asset(
                                        "assets/images/app_logo.png",
                                        height: SizeConfig.width * 0.25,
                                        width: SizeConfig.width * 0.25,
                                      ),
                                      SizedBox(
                                          height: SizeConfig.height * 0.03),
                                      Text(
                                        locale.get("LOG IN"),
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 24,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                // SizedBox(height: SizeConfig.height * 0.15),
                                Text(
                                  locale.get("Email Address"),
                                  style: TextStyles.heading1Style,
                                ),
                                SizedBox(height: SizeConfig.height * 0.005),
                                MainTextField(
                                  controller: model.emailController,
                                  hint: "Sherifzosar@Gmail.Com",
                                  suffixIcon: Padding(
                                    padding: const EdgeInsets.all(15),
                                    child: Icon(Icons.email_outlined),
                                  ),
                                  validator: Validator.email,
                                ),
                                SizedBox(height: SizeConfig.height * 0.01),
                                Text(
                                  locale.get("Password"),
                                  style: TextStyles.heading1Style,
                                ),
                                SizedBox(height: SizeConfig.height * 0.005),
                                MainTextField(
                                  controller: model.passwordController,
                                  hint: "****************",
                                  suffixIcon: Padding(
                                    padding: const EdgeInsets.all(15),
                                    child: Icon(Icons.lock_outline_rounded),
                                  ),
                                  validator: Validator.password,
                                ),
                                Wrap(
                                  alignment: WrapAlignment.spaceBetween,
                                  crossAxisAlignment: WrapCrossAlignment.center,
                                  // runAlignment: WrapAlignment.center,
                                  children: [
                                    Wrap(
                                      crossAxisAlignment:
                                          WrapCrossAlignment.center,
                                      alignment: WrapAlignment.center,
                                      runAlignment: WrapAlignment.center,
                                      children: [
                                        Checkbox(
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(3),
                                          ),
                                          value: model.isRememberMe,
                                          onChanged: (value) {
                                            model.isRememberMe = value!;
                                            model.setState();
                                          },
                                        ),
                                        Text(
                                          locale.get("Remember Me"),
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 12,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Align(
                                      alignment: Alignment.topRight,
                                      child: TextButton(
                                        style: TextButton.styleFrom(
                                          minimumSize: Size.zero,
                                          tapTargetSize:
                                              MaterialTapTargetSize.shrinkWrap,
                                        ),
                                        child: Text(
                                          locale.get("Forgot Your Password?"),
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 12,
                                            color: Colors.red,
                                            decoration:
                                                TextDecoration.underline,
                                          ),
                                        ),
                                        onPressed: () {
                                          AppHelper.push(
                                              context, ForgetPasswordPage());
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: SizeConfig.height * 0.05),
                                model.busy
                                    ? MainProgress()
                                    : MainButton(
                                        title: locale.get("Log in"),
                                        onPressed: model.submitFun,
                                      ),
                                LoginWithButtons(),
                                Center(
                                  child: Container(
                                    decoration: BoxDecoration(
                                        border: Border(
                                            bottom: BorderSide(
                                                color:
                                                    AppColors.primaryColor))),
                                    child: Wrap(
                                      crossAxisAlignment:
                                          WrapCrossAlignment.center,
                                      children: [
                                        Text(
                                          locale.get("Don't Have An Account?"),
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 10,
                                          ),
                                        ),
                                        TextButton(
                                          style: TextButton.styleFrom(
                                            minimumSize: Size.zero,
                                            // padding: EdgeInsets.zero,
                                            tapTargetSize: MaterialTapTargetSize
                                                .shrinkWrap,
                                          ),
                                          child: Text(
                                            locale.get("Create One"),
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 12,
                                            ),
                                          ),
                                          onPressed: () {
                                            AppHelper.push(
                                                context, SignupPage());
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
          ),
        );
      },
    );
  }
}

class SigninPageModel extends BaseModel {
  final BuildContext context;

  SigninPageModel({required this.context}) {
    emailController.text = "ahmed.ezzeldin.ibraim@gmail.com";
    passwordController.text = "123456789";
  }
  final formKey = GlobalKey<FormState>();
  var autovalidateMode = AutovalidateMode.disabled;
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  bool isRememberMe = false;

  void submitFun() {
    if (formKey.currentState!.validate()) {
      signin();
    } else {
      autovalidateMode = AutovalidateMode.always;
      setState();
    }
  }

  void signin() async {
    setBusy();
    final res = await auth.signIn(
      context,
      body: {
        "email": emailController.text,
        "password": passwordController.text,
        "remember_me": isRememberMe ? 1 : 0,
      },
    );
    if (res) {
      AppHelper.push(context, HomeNavPage());
    }
    setIdle();
  }
}
