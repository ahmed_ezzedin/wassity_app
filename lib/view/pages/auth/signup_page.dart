// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassity/core/models/city.dart';
import 'package:wassity/core/models/country.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/core/services/provider/provider_setup.dart';
import 'package:wassity/view/pages/nav_bar/home_nav_page.dart';
import 'package:wassity/view/styles/app_colors.dart';
import 'package:wassity/view/styles/text_styles.dart';
import 'package:wassity/view/view_helper/app_helper.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/widgets/buttons/main_button.dart';
import 'package:wassity/view/widgets/components/main_progress.dart';
import 'package:wassity/view/widgets/components/main_textfield.dart';
import 'package:wassity/view/widgets/components/main_textfield_dropdown.dart';
import 'package:wassity/view/widgets/language_buttons.dart';
import 'package:wassity/view/widgets/login_with_buttons.dart';

class SignupPage extends StatelessWidget {
  const SignupPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return BaseWidget<SignupPageModel>(
      model: SignupPageModel(context: context),
      builder: (_, model, child) {
        return Scaffold(
          body: SafeArea(
            child: model.busy
                ? MainProgress()
                : Container(
                    padding: const EdgeInsets.all(15),
                    width: double.infinity,
                    child: SingleChildScrollView(
                      child: Form(
                        key: model.formKey,
                        autovalidateMode: model.autovalidateMode,
                        child: Stack(
                          alignment: Alignment.topLeft,
                          children: [
                            Positioned(
                              top: -100,
                              left: -100,
                              child:
                                  SvgPicture.asset("assets/svgs/Group 290.svg"),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Center(
                                  child: Column(
                                    children: [
                                      LanguageButtons(),
                                      SizedBox(
                                          height: SizeConfig.height * 0.07),
                                      Image.asset(
                                        "assets/images/app_logo.png",
                                        height: SizeConfig.width * 0.25,
                                        width: SizeConfig.width * 0.25,
                                      ),
                                      SizedBox(
                                          height: SizeConfig.height * 0.03),
                                      Text(
                                        locale.get("SIGN UP"),
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 24,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                // SizedBox(height: SizeConfig.height * 0.15),
                                Text(
                                  locale.get("Your First Name"),
                                  style: TextStyles.heading1Style,
                                ),
                                SizedBox(height: SizeConfig.height * 0.005),
                                MainTextField(
                                  controller: model.firstNameController,
                                  hint: locale.get("First name"),
                                  suffixIcon: Padding(
                                      padding: const EdgeInsets.all(15),
                                      child: Icon(Icons.person_outline)),
                                  validator: Validator.required,
                                ),
                                Text(
                                  locale.get("Your Last Name"),
                                  style: TextStyles.heading1Style,
                                ),
                                SizedBox(height: SizeConfig.height * 0.005),
                                MainTextField(
                                  controller: model.lastNameController,
                                  hint: locale.get("Last name"),
                                  suffixIcon: Padding(
                                      padding: const EdgeInsets.all(15),
                                      child: Icon(Icons.person_outline)),
                                  validator: Validator.required,
                                ),
                                Text(
                                  locale.get("Email Address"),
                                  style: TextStyles.heading1Style,
                                ),
                                SizedBox(height: SizeConfig.height * 0.005),
                                MainTextField(
                                  controller: model.emailController,
                                  hint: "Sherifzosar@Gmail.Com",
                                  keyboardType: TextInputType.emailAddress,
                                  suffixIcon: Padding(
                                      padding: const EdgeInsets.all(15),
                                      child: Icon(Icons.email_outlined)),
                                  validator: Validator.email,
                                ),
                                SizedBox(height: SizeConfig.height * 0.005),
                                Text(
                                  locale.get("Password"),
                                  style: TextStyles.heading1Style,
                                ),
                                MainTextField(
                                  controller: model.passwordController,
                                  hint: "****************",
                                  suffixIcon: Padding(
                                    padding: const EdgeInsets.all(15),
                                    child: Icon(Icons.lock_outline_rounded),
                                  ),
                                  isObscure: false,
                                  validator: Validator.password,
                                ),
                                SizedBox(height: SizeConfig.height * 0.005),
                                Text(
                                  locale.get("Password Confirmation"),
                                  style: TextStyles.heading1Style,
                                ),
                                MainTextField(
                                  controller:
                                      model.passwordConfirmationController,
                                  hint: "****************",
                                  suffixIcon: Padding(
                                    padding: const EdgeInsets.all(15),
                                    child: Icon(Icons.lock_outline_rounded),
                                  ),
                                  isObscure: false,
                                  validator: Validator.isMatch,
                                  matchedValue: model.passwordController.text,
                                ),
                                SizedBox(height: SizeConfig.height * 0.005),
                                Text(
                                  locale.get("Your First Name"),
                                  style: TextStyles.heading1Style,
                                ),
                                MainTextField(
                                  controller: model.bioController,
                                  hint: locale.get("Bio"),
                                  suffixIcon: Padding(
                                      padding: const EdgeInsets.all(15),
                                      child: Icon(Icons.edit_outlined)),
                                  validator: Validator.required,
                                ),

                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Expanded(
                                      child: ListTile(
                                        title: Text(locale.get("Customer")),
                                        leading: Radio<String>(
                                          value: "customer",
                                          groupValue: model.userType,
                                          onChanged: (value) {
                                            model.userType = value;
                                            model.setState();
                                          },
                                          activeColor: Colors.green,
                                        ),
                                        onTap: () {
                                          model.userType = "customer";
                                          model.setState();
                                        },
                                      ),
                                    ),
                                    Expanded(
                                      child: ListTile(
                                        title: Text(locale.get("Dealer")),
                                        leading: Radio<String>(
                                          value: "dealer",
                                          groupValue: model.userType,
                                          onChanged: (value) {
                                            model.userType = "dealer";
                                            model.setState();
                                            model.getAllCountries();
                                            model.getAllCity();
                                          },
                                          activeColor: Colors.green,
                                        ),
                                        onTap: () {
                                          model.userType = "dealer";
                                          model.setState();
                                          model.getAllCountries();
                                          model.getAllCity();
                                        },
                                      ),
                                    ),
                                  ],
                                ),

                                model.userType == "customer"
                                    ? SizedBox()
                                    : Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            locale.get("Coin price"),
                                            style: TextStyles.heading1Style,
                                          ),
                                          SizedBox(
                                              height:
                                                  SizeConfig.height * 0.005),
                                          MainTextField(
                                            controller:
                                                model.coinPriceController,
                                            hint: "12.75 \$",
                                            keyboardType: TextInputType.number,
                                            suffixIcon: Padding(
                                                padding:
                                                    const EdgeInsets.all(15),
                                                child: Icon(Icons.money)),
                                            validator: Validator.number,
                                          ),
                                          model.loadCountries
                                              ? MainProgress()
                                              : MainTextFieldDropdown(
                                                  initialValue:
                                                      model.selectedCountry,
                                                  items: model.countriesList
                                                      .map((e) => e.name)
                                                      .toList(),
                                                  label: locale
                                                      .get("Select country"),
                                                  onChanged: (value) {
                                                    model.selectedCountry =
                                                        value;
                                                    model.setState();
                                                  },
                                                ),
                                          model.loadCities
                                              ? MainProgress()
                                              : MainTextFieldDropdown(
                                                  initialValue:
                                                      model.selectedCity,
                                                  items: model.citiesList
                                                      .map((e) => e.name)
                                                      .toList(),
                                                  label:
                                                      locale.get("Select city"),
                                                  onChanged: (value) {
                                                    model.selectedCity = value;
                                                    model.setState();
                                                  },
                                                ),
                                        ],
                                      ),
                                SizedBox(height: SizeConfig.height * 0.05),
                                MainButton(
                                  title: locale.get("Sign up"),
                                  onPressed: model.submitFun,
                                ),
                                LoginWithButtons(),

                                Center(
                                  child: Container(
                                    decoration: BoxDecoration(
                                        border: Border(
                                            bottom: BorderSide(
                                                color:
                                                    AppColors.primaryColor))),
                                    child: Wrap(
                                      crossAxisAlignment:
                                          WrapCrossAlignment.center,
                                      children: [
                                        Text(
                                          locale.get("Have Account?"),
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 10,
                                          ),
                                        ),
                                        TextButton(
                                          style: TextButton.styleFrom(
                                            minimumSize: Size.zero,
                                            // padding: EdgeInsets.zero,
                                            tapTargetSize: MaterialTapTargetSize
                                                .shrinkWrap,
                                          ),
                                          child: Text(
                                            locale.get("Login "),
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 12,
                                            ),
                                          ),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
          ),
        );
      },
    );
  }
}

class SignupPageModel extends BaseModel {
  final BuildContext context;
  SignupPageModel({required this.context});
  final formKey = GlobalKey<FormState>();
  var autovalidateMode = AutovalidateMode.disabled;
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final passwordConfirmationController = TextEditingController();
  final coinPriceController = TextEditingController();
  final bioController = TextEditingController();

  bool rememberMe = false;
  bool loadCities = false;
  bool loadCountries = false;
  String? userType = "customer";

  List<City> citiesList = [];
  List<Country> countriesList = [];
  String? selectedCity;
  String? selectedCountry;

  void submitFun() {
    if (formKey.currentState!.validate()) {
      signup();
    } else {
      autovalidateMode = AutovalidateMode.always;
      setState();
    }
  }
  
  void signup() async {
    setBusy();
    final res = await auth.signup(
      context,
      body: {
        "first_name": firstNameController.text,
        "last_name": lastNameController.text,
        "email": emailController.text,
        "password": passwordController.text,
        "password_confirmation": passwordConfirmationController.text,
        "type": userType,
        "bio": bioController.text,
        "coin_price": coinPriceController.text,
        "country_id": countriesList
            .firstWhere((element) => element.name == selectedCountry)
            .id,
        "city_id":
            citiesList.firstWhere((element) => element.name == selectedCity).id,
      },
    );
    if (res) {
      AppHelper.pushReplaceAll(context, HomeNavPage());
    }
    setIdle();
  }

  getAllCity() async {
    loadCities = true;
    setState();
    final res = await api.getAllCities(context);
    if (res != null) {
      if (res["data"]["data"] != null) {
        citiesList =
            res["data"]["data"].map<City>((e) => City.fromJson(e)).toList();
      }
    }
    loadCities = false;
    setState();
  }

  getAllCountries() async {
    loadCountries = true;
    setState();
    final res = await api.getAllCountries(context);
    if (res != null) {
      if (res["data"]["data"] != null) {
        countriesList = res["data"]["data"]
            .map<Country>((e) => Country.fromJson(e))
            .toList();
      }
    }
    loadCountries = false;
    setState();
  }
}
