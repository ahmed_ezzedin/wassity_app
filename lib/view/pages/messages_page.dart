import 'package:flutter/material.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/widgets/components/main_image.dart';

class MessagesPage extends StatelessWidget {
  const MessagesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return BaseWidget<MessagesPageModel>(
      model: MessagesPageModel(context: context),
      builder: (_, model, child) {
        return Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.white,
            foregroundColor: Colors.black,
            title: Text(
              locale.get("Messages"),
              style: const TextStyle(
                color: Colors.black,
              ),
            ),
          ),
          body: Container(
            padding: const EdgeInsets.all(10),
            child: ListView.builder(
              itemCount: 10,
              itemBuilder: (context, index) {
                return buildMessageItem();
              },
            ),
          ),
        );
      },
    );
  }

  ListTile buildMessageItem() {
    return ListTile(
      leading: const MainImage(
        imageType: ImageType.network,
        imagePath:
            'https://corporatephotographymelbourne.com.au/wp-content/uploads/2019/05/Corporate-suit-business-man-lawyer-law-photography-melbourne-5-1.jpg',
        isCircle: true,
        height: 50,
        width: 50,
        boxFit: BoxFit.cover,
      ),
      title: const Text(
        "Inquiries About Completing A Currency Exchange Deal",
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 12,
        ),
      ),
      subtitle: Column(
        // mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
              "Peace be upon you. I need to explain to you the project, the scope of work and the final agreementWhen is the right time today for a Zoom session? The project is available in user flow, explanation and xd for most pages except for the landing page Waiting for you"),
          Wrap(
            crossAxisAlignment: WrapCrossAlignment.center,
            children: const [
              Icon(
                Icons.access_time_rounded,
                color: Colors.grey,
              ),
              SizedBox(width: 10),
              Text(
                "2 Days And 11 Minutes",
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}

class MessagesPageModel extends BaseModel {
  final BuildContext context;

  MessagesPageModel({required this.context});
}
