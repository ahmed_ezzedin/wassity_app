import 'package:flutter/material.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/view/pages/main_page.dart';
import 'package:wassity/view/view_helper/app_helper.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'dart:math' as math;

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> with TickerProviderStateMixin {
  late TickerProvider vsync;
  @override
  void initState() {
    super.initState();
    vsync = this;
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<SplashPageModel>(
      model: SplashPageModel(context: context, vsync: vsync),
      builder: (context, model, child) {
        return Scaffold(
          body: SizedBox(
            width: double.infinity,
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizeTransition(
                    sizeFactor: model.animation,
                    axisAlignment: 0,
                    child: Center(
                      child: AnimatedBuilder(
                        animation: model._controller,
                        builder: (_, child) {
                          return Transform.rotate(
                            angle: model._controller.value * 2 * math.pi,
                            child: child,
                          );
                        },
                        child: Image.asset(
                          "assets/images/app_logo.png",
                          height: SizeConfig.width * 0.5,
                          width: SizeConfig.width * 0.5,
                        ),
                      ),
                    ),
                  ),
                  // AnimatedBuilder(
                  //   animation: _controller,
                  //   builder: (_, child) {
                  //     return Transform.rotate(
                  //       angle: _controller.value * 2 * math.pi,
                  //       child: child,
                  //     );
                  //   },
                  //   child: Image.asset(
                  //     "assets/images/app_logo.png",
                  //     height: SizeConfig.width * 0.25,
                  //     width: SizeConfig.width * 0.25,
                  //   ),
                  // ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

class SplashPageModel extends BaseModel {
  final TickerProvider vsync;
  final BuildContext context;
  SplashPageModel({required this.context, required this.vsync}) {
    initController();
  }

  late final AnimationController _controller;
  late Animation<double> animation;
  initController() {
    _controller = AnimationController(
      vsync: vsync,
      duration: const Duration(milliseconds: 1500),
    )..forward().whenComplete(() {
        delayFun();
      });
    animation = Tween<double>(begin: 0.0, end: 1).animate(_controller);
  }

  void delayFun() {
    Future.delayed(
      const Duration(milliseconds: 200),
      () async {
        AppHelper.pushReplaceAll(context, const MainPage());
      },
    );
  }
}
