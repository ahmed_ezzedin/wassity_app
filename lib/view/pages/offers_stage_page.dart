// ignore_for_file: unnecessary_string_escapes

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassity/core/models/project.dart';
import 'package:wassity/core/models/rate.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/core/services/provider/provider_setup.dart';
import 'package:wassity/view/styles/app_colors.dart';
import 'package:wassity/view/styles/text_styles.dart';
import 'package:wassity/view/view_helper/app_helper.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/widgets/buttons/main_button.dart';
import 'package:wassity/view/widgets/components/main_textfield.dart';
import 'package:wassity/view/widgets/review_widget.dart';

class OfferStagePage extends StatelessWidget {
  final Project project;
  const OfferStagePage({Key? key, required this.project}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseWidget<OfferStagePageModel>(
      model: OfferStagePageModel(context: context),
      builder: (_, model, child) {
        final locale = AppLocalizations.of(context);
        return Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.white,
            foregroundColor: Colors.black,
            title: Text(
              "${project.title}",
              style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          body: SafeArea(
            child: Container(
              padding: const EdgeInsets.all(10),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: SizeConfig.height * 0.03),
                    Text(
                      locale.get("Start Building Your Deal"),
                      style: const TextStyle(
                        fontWeight: FontWeight.w900,
                        fontSize: 24,
                      ),
                    ),
                    SizedBox(height: SizeConfig.height * 0.01),
                    Wrap(
                      alignment: WrapAlignment.center,
                      crossAxisAlignment: WrapCrossAlignment.center,
                      runAlignment: WrapAlignment.center,
                      children: [
                        Text(
                          locale.get("Wewe Currency Exchange Deal"),
                          style: const TextStyle(
                            color: Colors.grey,
                          ),
                        ),
                        const SizedBox(width: 10),
                        SvgPicture.asset("assets/svgs/money (1).svg"),
                      ],
                    ),
                    SizedBox(height: SizeConfig.height * 0.05),
                    Theme(
                      data: ThemeData(
                          colorScheme: const ColorScheme.light(primary: AppColors.primaryColor)
                              .copyWith(secondary: AppColors.primaryColor)),
                      // data: ThemeData(
                      //   canvasColor: AppColors.primaryColor,
                      //   primaryColor: AppColors.primaryColor,
                      //   colorScheme: ColorScheme.fromSwatch()
                      //       .copyWith(secondary: AppColors.primaryColor),
                      // ),
                      child: Stepper(
                        controlsBuilder: (context, controlsDetails) {
                          return const SizedBox();
                        },
                        type: StepperType.vertical,
                        steps: [
                          Step(
                              title: Text(locale.get("Offer is pending")),
                              content: const SizedBox(),
                              isActive: project.status == "pending",
                              state: StepState.complete),
                          Step(
                              title: Text(locale.get("Stage Of Receiving Offers")),
                              content: const SizedBox(),
                              isActive: project.status == "recieve_offer",
                              state: StepState.complete),
                          Step(
                              title: Text(locale.get("Implementation Phase")),
                              content: const SizedBox(),
                              isActive: project.status == "implementation" || project.status == "accepted",
                              state: StepState.complete),
                          Step(
                              title: Text(locale.get("Delivery Stage")),
                              content: const SizedBox(),
                              isActive: project.status == "delivery",
                              state: StepState.complete),
                        ],
                      ),
                    ),
                    // SizedBox(height: SizeConfig.height * 0.05),
                    // Text(
                    //   locale.get("Transaction Status"),
                    //   style: const TextStyle(
                    //     color: Colors.grey,
                    //   ),
                    // ),
                    Text(
                      locale.get("Project Details"),
                      style: const TextStyle(
                        color: Colors.grey,
                        fontSize: 20,
                      ),
                    ),

                    SizedBox(height: SizeConfig.height * 0.02),
                    Text(
                      locale.get("${project.description}"),
                      style: const TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    SizedBox(height: SizeConfig.height * 0.02),
                    if (auth.userModel!.user!.type == "dealer")
                      Form(
                        key: model.formKey,
                        autovalidateMode: model.autovalidateMode,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              locale.get("Add your Offer Now"),
                              style: const TextStyle(
                                color: Colors.grey,
                                fontSize: 20,
                              ),
                            ),
                            SizedBox(height: SizeConfig.height * 0.02),
                            Text(
                              locale.get(
                                  "Digital Currency (Digital Currency And Digital Money) Is A Type Of Digital Currency And Digital Cash. Physical, Physical, Portable, Portable, Portable, Portable, Portable, Portable Mutual, Foreign Exchange Exchange. On The Same Front, You Can Use Currencies In Forex Trading. Digital Currency (Digital Currency And Digital Money) Is A Type Of Digital Currency And Digital Cash. Physical, Physi"),
                              style: const TextStyle(
                                fontSize: 13,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            SizedBox(height: SizeConfig.height * 0.01),
                            Text(
                              locale.get("Offer Title"),
                              style: TextStyles.heading1Style,
                            ),
                            SizedBox(height: SizeConfig.height * 0.005),
                            MainTextField(
                              controller: model.titleController,
                              hint: locale.get("Offer Title"),
                              validator: Validator.required,
                            ),
                            SizedBox(height: SizeConfig.height * 0.01),
                            Text(
                              locale.get("Offer Value"),
                              style: TextStyles.heading1Style,
                            ),
                            SizedBox(height: SizeConfig.height * 0.005),
                            MainTextField(
                                controller: model.offerValueController,
                                hint: locale.get("Offer Value"),
                                validator: Validator.number,
                                keyboardType: TextInputType.number,
                                suffixIcon: const Icon(Icons.local_offer_outlined, color: Colors.grey)),
                            SizedBox(height: SizeConfig.height * 0.01),
                            Text(
                              locale.get("The Amount to be transferred"),
                              style: TextStyles.heading1Style,
                            ),
                            SizedBox(height: SizeConfig.height * 0.005),
                            MainTextField(
                                controller: model.deusController,
                                hint: locale.get("Your Dues"),
                                validator: Validator.number,
                                keyboardType: TextInputType.number,
                                suffixIcon: const Icon(Icons.local_offer_outlined, color: Colors.grey)),
                            SizedBox(height: SizeConfig.height * 0.01),
                            Text(
                              locale.get("Details of the transactions"),
                              style: TextStyles.heading1Style,
                            ),
                            SizedBox(height: SizeConfig.height * 0.005),
                            MainTextField(
                                controller: model.detailsController,
                                hint: locale.get("Details of the transactions"),
                                validator: Validator.required,
                                maxLines: 10,
                                suffixIcon: const Icon(Icons.edit_outlined, color: Colors.grey)),
                            MainButton(
                              title: locale.get("Create Offer"),
                              onPressed: () {},
                            ),
                          ],
                        ),
                      ),
                    SizedBox(height: SizeConfig.height * 0.02),
                    Text(
                      locale.get("Offers On Your Deal"),
                      style: const TextStyle(
                        color: Colors.grey,
                        fontSize: 20,
                      ),
                    ),
                    SizedBox(height: SizeConfig.height * 0.02),
                    model.isLoadRate
                        ? const SizedBox()
                        : Column(
                            children: model.rateList.map(
                              (e) {
                                return  ReviewWidget(rate: e);
                              },
                            ).toList(),
                          ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class OfferStagePageModel extends BaseModel {
  final BuildContext context;
  OfferStagePageModel({required this.context}){
    getRateIndex();
  }
  bool isLoadRate = false;
  final formKey = GlobalKey<FormState>();
  var autovalidateMode = AutovalidateMode.disabled;
  List<Rate> rateList = [];

  final titleController = TextEditingController();
  final offerValueController = TextEditingController();
  final deusController = TextEditingController();
  final detailsController = TextEditingController();

  int currentStep = 0;
  Map<String, dynamic> stepperMap = {};

  void submitFun() {
    if (formKey.currentState!.validate()) {
      createOffer();
    } else {
      autovalidateMode = AutovalidateMode.always;
      setState();
    }
  }

  void createOffer() async {
    setBusy();
    final res = await api.createOffer(
      context,
      body: {
        "title": titleController.text,
        "temp": deusController.text,
        "value": offerValueController.text,
        "project_id": deusController.text,
        "details": detailsController.text,
      },
    );
    if (res["data"] != null) {
      AppHelper.showToastMesssage("Offer created successfully");
    }
    setIdle();
  }

  getRateIndex() async {
    isLoadRate = true;
    setState();
    final res = await api.getRateIndex(
      context,
      body: {
        "limit": 1,
        "offset": 1,
        "field": "id",
        "sort": "ASC",
        "resource": "all",
        "deleted": "false",
        "paginate": "false",
        "columns": ["rateable_type", "rateable_id"],
        "operand": ["=", "="],
        "column_values": ["App\Models\Project", 5]
      },
    );
    if (res["success"] == true) {
      if (res["data"]["data"] != null) {
        rateList = res["data"]["data"].map<Rate>((e) => Rate.fromJson(e)).toList();
      }
    }
    isLoadRate = false;
    setState();
  }
}
