import 'package:flutter/material.dart';
import 'package:wassity/core/models/notification.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/core/services/provider/provider_setup.dart';
import 'package:wassity/view/view_helper/app_helper.dart';
import 'package:wassity/view/widgets/components/main_image.dart';
import 'package:wassity/view/widgets/components/main_progress.dart';

class NotificationsPage extends StatelessWidget {
  const NotificationsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return BaseWidget<NotificationsPageModel>(
      model: NotificationsPageModel(context: context),
      builder: (_, model, child) {
        return Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.white,
            foregroundColor: Colors.black,
            title: Text(
              locale.get("Notifications"),
              style: const TextStyle(
                color: Colors.black,
              ),
            ),
          ),
          body: model.busy
              ? const MainProgress()
              : Container(
                  padding: const EdgeInsets.all(10),
                  child: ListView.builder(
                    itemCount: model.notificationsList.length,
                    itemBuilder: (context, index) {
                      return buildNotificationItem(
                          model.notificationsList[index]);
                    },
                  ),
                ),
        );
      },
    );
  }

  ListTile buildNotificationItem(NotificationModel notification) {
    return ListTile(
      leading: MainImage(
        imageType: ImageType.network,
        // 'https://corporatephotographymelbourne.com.au/wp-content/uploads/2019/05/Corporate-suit-business-man-lawyer-law-photography-melbourne-5-1.jpg',
        imagePath: "${notification.data!.image}",
        isCircle: true,
        height: 50,
        width: 50,
        boxFit: BoxFit.cover,
      ),
      title: Text(
        "${notification.data!.title}",
        style: const TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 12,
        ),
      ),
      subtitle: Column(
        // mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
              // "Peace be upon you. I need to explain to you the project, the scope of work and the final agreementWhen is the right time today for a Zoom session? The project is available in user flow, explanation and xd for most pages except for the landing page Waiting for you",
              "${notification.data!.description}"),
          Wrap(
            crossAxisAlignment: WrapCrossAlignment.center,
            children:  [
            const  Icon(
                Icons.access_time_rounded,
                color: Colors.grey,
              ),
             const  SizedBox(width: 10),
              Text(
                // "2 Days And 11 Minutes",
                AppHelper.formatDateTime(DateTime.parse(notification.data!.updatedAt! ) , pattern: "h:mm a d MMM y"),
                style:const TextStyle(
                  fontWeight: FontWeight.w500,
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  // ListTile buildNotificationItem() {
  //   return ListTile(
  //     leading: const MainImage(
  //       imageType: ImageType.network,
  //       imagePath:
  //           'https://corporatephotographymelbourne.com.au/wp-content/uploads/2019/05/Corporate-suit-business-man-lawyer-law-photography-melbourne-5-1.jpg',
  //       isCircle: true,
  //       height: 50,
  //       width: 50,
  //       boxFit: BoxFit.cover,
  //     ),
  //     title: const Text(
  //       "You Were Chosen By Khaled To Complete His Deal",
  //       style: TextStyle(
  //         fontWeight: FontWeight.bold,
  //         fontSize: 12,
  //       ),
  //     ),
  //     subtitle: Wrap(
  //       crossAxisAlignment: WrapCrossAlignment.center,
  //       children: const [
  //         Icon(
  //           Icons.access_time_rounded,
  //           color: Colors.grey,
  //         ),
  //         SizedBox(width: 10),
  //         Text(
  //           "2 Days And 11 Minutes",
  //           style: TextStyle(
  //             fontWeight: FontWeight.w500,
  //           ),
  //         )
  //       ],
  //     ),
  //   );
  // }
}

class NotificationsPageModel extends BaseModel {
  final BuildContext context;

  NotificationsPageModel({required this.context}) {
    getAllNotifications();
  }

  List<NotificationModel> notificationsList = [];

  getAllNotifications() async {
    setBusy();
    final res = await api.getAllNotifications(context);
    if (res["success"] == true) {
      if (res["data"]["data"] != null) {
        notificationsList = res["data"]["data"]
            .map<NotificationModel>((e) => NotificationModel.fromJson(e))
            .toList();
      }
    }
    setIdle();
  }
}
