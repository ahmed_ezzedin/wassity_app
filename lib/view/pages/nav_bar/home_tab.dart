import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassity/core/models/mediator.dart';
import 'package:wassity/core/models/project.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/core/services/core_helper/global_var.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/core/services/provider/provider_setup.dart';
import 'package:wassity/view/pages/add_project_page.dart';
import 'package:wassity/view/styles/app_colors.dart';
import 'package:wassity/view/view_helper/app_helper.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/widgets/buttons/main_dropdown_button.dart';
import 'package:wassity/view/widgets/components/main_progress.dart';
import 'package:wassity/view/widgets/mediator_item.dart';
import 'package:wassity/view/widgets/my_project_item.dart';

class HomeTab extends StatelessWidget {
  const HomeTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseWidget<HomeTabModel>(
      model: HomeTabModel(context: context),
      builder: (_, model, child) {
        final locale = AppLocalizations.of(context);
        return Scaffold(
          body: Container(
            margin: const EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  locale.get("Currency Exchange"),
                  style: const TextStyle(
                    fontWeight: FontWeight.w900,
                    fontSize: 24,
                  ),
                ),
                SizedBox(height: SizeConfig.height * 0.01),
                Wrap(
                  alignment: WrapAlignment.center,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  runAlignment: WrapAlignment.center,
                  children: [
                    Text(
                      locale.get("Exchange Your Currency Faster"),
                      style: const TextStyle(
                        // fontWeight: FontWeight.w900,
                        // fontSize: 24,
                        color: Colors.grey,
                      ),
                    ),
                    const SizedBox(width: 10),
                    SvgPicture.asset("assets/svgs/money (1).svg"),
                  ],
                ),
                SizedBox(height: SizeConfig.height * 0.05),
                buildSwitchButtons(model, locale),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ElevatedButton(
                      child: Wrap(
                        children: [
                          SvgPicture.asset("assets/svgs/add-button.svg"),
                          Text(
                            locale.get("Add A Transfer Request"),
                            style: const TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                            ),
                          ),
                          const SizedBox(width: 10),
                        ],
                      ),
                      onPressed: model.addProjectPressed,
                    ),
                    MainDropdownButton(
                      value: model.selectedValue,
                      items: model.sortList,
                      onChanged: (value) {
                        model.selectedValue = value;
                        model.setState();
                      },
                    ),
                  ],
                ),
                SizedBox(height: SizeConfig.height * 0.02),
                model.busy
                    ? const MainProgress()
                    : model.isMyProj
                        ? Expanded(
                            child: ListView.builder(
                              itemCount: GlobalVar.homeProjectsList.length,
                              itemBuilder: (context, index) {
                                return MyProjectsItem(
                                  project: GlobalVar.homeProjectsList[index],
                                );
                              },
                            ),
                          )
                        : Expanded(
                            child: ListView.builder(
                              itemCount: GlobalVar.homeMediatorList.length,
                              itemBuilder: (context, index) {
                                return MediatorItem(mediator: GlobalVar.homeMediatorList[index], model: model);
                              },
                            ),
                          ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget buildSwitchButtons(HomeTabModel model, AppLocalizations locale) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        InkWell(
          child: Column(
            children: [
              Container(
                width: SizeConfig.width * 0.4,
                padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                decoration: BoxDecoration(
                  color: model.isMyProj ? AppColors.primaryColor.withAlpha(50) : Colors.grey.withAlpha(50),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Wrap(
                  children: [
                    SvgPicture.asset("assets/svgs/Group 206.svg"),
                    const SizedBox(width: 10),
                    Text(
                      locale.get("My Projects"),
                      style: TextStyle(
                        color: model.isMyProj ? AppColors.primaryColor : Colors.grey,
                        // fontSize: 13,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              model.isMyProj
                  ? const Icon(
                      Icons.arrow_drop_down,
                      color: AppColors.primaryColor,
                    )
                  : const SizedBox(height: 24),
            ],
          ),
          onTap: () {
            // model.getMyProjects();
            model.getProjectsIndex();
          },
        ),
        const SizedBox(),
        InkWell(
          child: Column(
            children: [
              Container(
                width: SizeConfig.width * 0.4,
                padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                decoration: BoxDecoration(
                  color: model.isMyProj ? Colors.grey.withAlpha(70) : AppColors.primaryColor.withAlpha(50),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Wrap(
                  children: [
                    SvgPicture.asset("assets/svgs/Repeat Grid 2.svg"),
                    const SizedBox(width: 10),
                    Text(
                      locale.get("Mediators"),
                      style: TextStyle(
                        color: model.isMyProj ? Colors.grey : AppColors.primaryColor,
                        // fontSize: 12,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              model.isMyProj
                  ? const SizedBox(height: 24)
                  : const Icon(
                      Icons.arrow_drop_down,
                      color: AppColors.primaryColor,
                    ),
            ],
          ),
          onTap: () {
            model.getUsers();
          },
        ),
      ],
    );
  }
}

class HomeTabModel extends BaseModel {
  final BuildContext context;
  HomeTabModel({required this.context}) {
    if (GlobalVar.homeProjectsList.isEmpty) {
      // getMyProjects();
      getProjectsIndex();
    }
  }
  bool isMyProj = true;
  String selectedValue = "New";
  List<String> sortList = [
    "New",
    "Highest rated",
    "Old",
  ];

  // getMyProjects() async {
  //   isMyProj = true;
  //   setState();
  //   // if (GlobalVar.homeProjectsList.isEmpty) {
  //   setBusy();
  //   final res = await api.getAllProjects(context);
  //   if (res["success"] == true) {
  //     if (res["data"]["data"] != null) {
  //       GlobalVar.homeProjectsList = res["data"]["data"].map<Project>((e) => Project.fromJson(e)).toList();
  //     }
  //   }
  //   setIdle();
  //   // }
  // }

  getProjectsIndex() async {
    isMyProj = true;
    setState();
    setBusy();
    final res = await api.getProjectsIndex(
      context,
      body: {
        "limit": "1",
        "offset": "1",
        "field": "id",
        "sort": "ASC",
        "resource": "all",
        "deleted": "false",
        "paginate": "false",
        "columns": ["status"],
        "operand": ["="],
        "column_values": ["recieve_offer"]
      },
    );
    if (res["success"] == true) {
      if (res["data"]["data"] != null) {
        GlobalVar.homeProjectsList = res["data"]["data"].map<Project>((e) => Project.fromJson(e)).toList();
      }
    }
    setIdle();
  }

  getUsers() async {
    isMyProj = false;
    setState();
    if (GlobalVar.homeMediatorList.isEmpty) {
      setBusy();
      final res = await api.getAllUsers(context);
      if (res["success"] == true) {
        if (res["data"]["data"] != null) {
          GlobalVar.homeMediatorList = res["data"]["data"].map<Mediator>((e) => Mediator.fromJson(e)).toList();
        }
      }
      setIdle();
    }
  }

  void addProjectPressed() async {
    final result = await AppHelper.push(context, const AddProjectsPage());
    if (result != null) {
      isMyProj = true;
      setState();
      setBusy();
      final res = await api.getAllProjects(context);
      if (res["success"] == true) {
        if (res["data"]["data"] != null) {
          GlobalVar.homeProjectsList = res["data"]["data"].map<Project>((e) => Project.fromJson(e)).toList();
        }
      }
      setIdle();
    }
  }

  void hire({required int userId, required int projectId}) async {
    setBusy();
    final res = await api.hireOffer(
      context,
      body: {
        "user_id": userId,
        "id": projectId,
      },
    );
    if (res["success"] == true) {
      Navigator.of(context).pop();
      AppHelper.showSnackBarMessage(context, message: "Hire request sent");
    }
    setIdle();
  }
}
