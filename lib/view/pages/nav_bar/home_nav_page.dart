import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/core/services/core_helper/launcher.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/pages/drawer_page.dart';
import 'package:wassity/view/pages/messages_page.dart';
import 'package:wassity/view/pages/nav_bar/favorite_tab.dart';
import 'package:wassity/view/pages/nav_bar/home_tab.dart';
import 'package:wassity/view/pages/nav_bar/profile_tab.dart';
import 'package:wassity/view/pages/notifications_page.dart';
import 'package:wassity/view/styles/app_colors.dart';
import 'package:wassity/view/view_helper/app_helper.dart';
import 'package:wassity/view/widgets/dialogs/exit_dialog.dart';
import 'package:wassity/view/widgets/dialogs/language_dialog.dart';

// ignore: use_key_in_widget_constructors
class HomeNavPage extends StatelessWidget {
  final List<Widget> listTabs = [
    const HomeTab(),
    // const CartTab(),
    const FavoriteTab(),
    const ProfileTab(),
  ];


  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
      final appLanguage = Provider.of<AppLanguage>(context);
    return BaseWidget<HomeNavPageModel>(
      model: HomeNavPageModel(context: context, locale: locale, appLanguage: appLanguage ),
      builder: (_, model, child) {
        return WillPopScope(
           onWillPop: model.onWillPop,
          child: Scaffold(
            key: model.scaffoldKey,
            appBar: AppBar(
              elevation: 0,
              backgroundColor: Colors.white,
              leading: Padding(
                padding: const EdgeInsets.all(10),
                child: IconButton(
                  icon: SvgPicture.asset("assets/svgs/Group 309.svg"),
                  onPressed: () {
                    model.scaffoldKey.currentState!.openDrawer();
                  },
                ),
              ),
              actions: [
                IconButton(
                  icon: const Icon(
                    Icons.notifications_outlined,
                    color: Colors.grey,
                  ),
                  onPressed: () {
                    AppHelper.push(context, const NotificationsPage());
                  },
                ),
                IconButton(
                  icon: const Icon(
                    Icons.email_outlined,
                    color: Colors.grey,
                  ),
                  onPressed: () {
                    AppHelper.push(context, const MessagesPage());
                  },
                ),
              ],
            ),
            drawer: DrawerPage(model: model),
            body: listTabs[model.pageIndex],
            bottomNavigationBar: AnimatedContainer(
              duration: const Duration(milliseconds: 400),
              height: model.isVisible ? 55 : 0,
              child: Wrap(
                children: [
                  BottomNavigationBar(
                    // items: navigationBarItem(model),
                    items: [
                      BottomNavigationBarItem(
                        icon: Icon(
                          Icons.home_outlined,
                          color: model.pageIndex == 0
                              ? AppColors.primaryColor
                              : Colors.grey,
                        ),
                        label: locale.get('Home'),
                      ),
                      // BottomNavigationBarItem(
                      //   icon: Icon(
                      //     Icons.shopping_bag_outlined,
                      //     color: model.pageIndex == 1
                      //         ? AppColors.primaryColor
                      //         : Colors.grey,
                      //   ),
                      //   label: locale.get('Cart'),
                      // ),
                      BottomNavigationBarItem(
                        icon: Icon(
                          Icons.favorite_outline,
                          color: model.pageIndex == 1
                              ? AppColors.primaryColor
                              : Colors.grey,
                        ),
                        label: locale.get('Favorite'),
                      ),
                      BottomNavigationBarItem(
                        icon: Icon(
                          Icons.person_outline_rounded,
                          color: model.pageIndex == 2
                              ? AppColors.primaryColor
                              : Colors.grey,
                        ),
                        label: locale.get('Profile'),
                      ),
                    ],
                    elevation: 0,
                    currentIndex: model.pageIndex,
                    selectedItemColor: AppColors.primaryColor,
                    unselectedItemColor: Colors.black,
                    showUnselectedLabels: true,
                    showSelectedLabels: true,
                    selectedLabelStyle: const TextStyle(
                      fontWeight: FontWeight.bold,
                      color: AppColors.primaryColor,
                    ),
                    unselectedLabelStyle: const TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                    onTap: model.changePageIndex,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

class HomeNavPageModel extends BaseModel {
  final BuildContext context;
  final AppLocalizations locale;
  final AppLanguage appLanguage;
  HomeNavPageModel({required this.context, required this.locale  , required this.appLanguage});

  final TextEditingController searchController = TextEditingController();

  int pageIndex = 0;
  bool isVisible = true;
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  changePageIndex(int index) {
    AppHelper.appPrint(index);
    pageIndex = index;
    setState();
  }
    void rateUsFun() {
    const String androidId = "com.cloudsecrets.theconceptapplication";
    Launcher.launcherFun(
            "https://play.google.com/store/apps/details?id= $androidId",
            LaunchType.url,
          );     
  }
    Future<bool> onWillPop() async {
    final result = await showDialog(
      context: context,
      builder: (ctx) {
        return ExitDialog(context: context);
      },
    );
    if (result != null) {
      return true;
    }
    return false;
  }


    void languageFun() async {
    var result = await showDialog(
      context: context,
      builder: (ctx) {
        return LanguageDialog(context: context);
      },
    );
    if (result != null) {
      if (result == "English") {
        appLanguage.changeLanguage(const Locale("en"));
      } else {
        appLanguage.changeLanguage(const Locale("ar"));
      }
    }
  }
}
