import 'package:flutter/material.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/core/services/localization/app_localization.dart';

class CartTab extends StatelessWidget {
  const CartTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return BaseWidget<CartTabModel>(
      model: CartTabModel(context: context),
      builder: (_, model, child) {
        return Scaffold(
          body: SizedBox(
            width: double.infinity,
            child: SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children:  [
                    Text(locale.get("Cart Tab")),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class CartTabModel extends BaseModel {
  final BuildContext context;
  CartTabModel({required this.context});
}
