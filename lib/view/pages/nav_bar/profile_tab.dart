import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/core/services/provider/provider_setup.dart';
import 'package:wassity/view/styles/app_colors.dart';
import 'package:wassity/view/view_helper/dialog_helper.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/widgets/components/main_image.dart';
import 'package:wassity/view/widgets/components/stars_rating_widget.dart';

class ProfileTab extends StatelessWidget {
  const ProfileTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return BaseWidget<ProfileTabModel>(
      model: ProfileTabModel(context: context),
      builder: (_, model, child) {
        return Scaffold(
          body: SizedBox(
            width: double.infinity,
            child: SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(height: SizeConfig.height * 0.01),
                    Align(
                      alignment: Alignment.topRight,
                      child: ElevatedButton(
                        child:  Text(locale.get("Edit")),
                        onPressed: () {
                          DialogHelper.editDialog(context);
                        },
                      ),
                    ),
                   auth.userModel?.user?.image == null
                      ? Container(
                          height: 80,
                          width: 80,
                          child: const Icon(
                            Icons.person_outline,
                            size: 40,
                            color: Colors.white,
                          ),
                          decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.grey,
                          ),
                        )
                      : MainImage(
                          imageType: ImageType.network,
                          imagePath: "${auth.userModel!.user!.image}",
                          isCircle: true,
                          height: 80,
                          width: 80,
                          boxFit: BoxFit.cover,
                        ),
                    SizedBox(height: SizeConfig.height * 0.02),
                    Text(
                      "${auth.userModel!.user!.firstName} ${auth.userModel!.user!.lastName}",
                      style: const TextStyle(
                        fontWeight: FontWeight.w900,
                        fontSize: 26,
                      ),
                    ),
                    SizedBox(height: SizeConfig.height * 0.03),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Wrap(
                          children: [
                            SvgPicture.asset("assets/svgs/pin.svg"),
                            const SizedBox(width: 10),
                            Text(
                              "${locale.get("From")} ${auth.userModel!.user!.city} ",
                              style: TextStyle(
                                color: Colors.grey.shade700,
                                fontWeight: FontWeight.w500,
                                // fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                        Wrap(
                          crossAxisAlignment: WrapCrossAlignment.center,
                          children: [
                            // SvgPicture.asset("assets/svgs/Repeat Grid 1.svg"),
                            // SvgPicture.asset("assets/svgs/pin.svg"),
                            Icon(
                              Icons.star,
                              color: Colors.yellow.shade600,
                              size: 20,
                            ),
                            const SizedBox(width: 10),
                             Text(
                              "${auth.userModel!.user!.rate}/5",
                              style:const TextStyle(
                                fontWeight: FontWeight.w500,
                                // fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                        const Text(
                          "CEO IQ2LIFE",
                          style: TextStyle(
                            color: AppColors.primaryColor,
                            fontWeight: FontWeight.w900,
                            fontSize: 14,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: SizeConfig.height * 0.02),
                    Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: [
                        const Icon(
                          Icons.access_time,
                          color: Colors.grey,
                          // size: 20,
                        ),
                        const SizedBox(width: 10),
                        Text(
                          "Avg. Response Time  1h",
                          style: TextStyle(
                            color: Colors.grey.shade700,
                            fontWeight: FontWeight.w500,
                            // fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: SizeConfig.height * 0.05),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                         Text(
                          locale.get("Brief About Me"),
                          style:const  TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        SizedBox(height: SizeConfig.height * 0.01),
                         Text(
                          // "Hi. I Am Khaled, I Have A Bachelor’s Degree In Information Systems, And I Have Been Working As A User Interface And User Experience Designer For Two Years, And I Have Many Projects That I Designed. I Consider Your Design As My Design Because It Simply Speaks About Me And The Degree Of Professionalism For My Work. I Can Do The Following Professionally And Quickly I Have Experience In Hi. I Am Sherif Ashraf, I Have A Bachelor’s Degree In Information Systems, And I Have Been Working As A User Interface And User Experience Designer For Two Years, And I Have Many Projects That I Designed. I Consider Your Design As My Design Because It Simply Speaks A",
                          "${auth.userModel!.user!.bio}",
                          style:const  TextStyle(
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        SizedBox(height: SizeConfig.height * 0.05),
                         Text(
                          locale.get("Customer Reviews And Opinions"),
                          style:const  TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        SizedBox(height: SizeConfig.height * 0.01),
                        Wrap(
                          children: [
                            StarsRatingWidget(
                              spacing: 0,
                              rating: 5,
                              color: Colors.yellow.shade700,
                              iconSize: 20,
                            ),
                            const SizedBox(width: 10),
                            const Text("( 5/5 )"),
                            const SizedBox(width: 10),
                             Text("( 700 ${locale.get("Rating")} )"),
                          ],
                        ),
                        SizedBox(height: SizeConfig.height * 0.01),
                        // Column(
                        //   children:
                        //       List.generate(5, (index) => const ReviewWidget()),
                        // ),
                        // SizedBox(height: SizeConfig.height * 0.02),
                        // Center(
                        //   child: TextButton(
                        //     child:  Text(
                        //       locale.get("Show more"),
                        //       style:const  TextStyle(
                        //           color: Colors.black,
                        //           fontWeight: FontWeight.w400),
                        //     ),
                        //     onPressed: () {},
                        //   ),
                        // ),
                        SizedBox(height: SizeConfig.height * 0.02),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class ProfileTabModel extends BaseModel {
  final BuildContext context;
  ProfileTabModel({required this.context});
}
