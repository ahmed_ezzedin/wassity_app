import 'package:flutter/material.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/core/services/provider/provider_setup.dart';
import 'package:wassity/view/pages/about_page.dart';
import 'package:wassity/view/pages/auth/change_password_page.dart';
import 'package:wassity/view/pages/main_page.dart';
import 'package:wassity/view/pages/nav_bar/home_nav_page.dart';
import 'package:wassity/view/pages/splash_page.dart';
import 'package:wassity/view/view_helper/app_helper.dart';
import 'package:wassity/view/view_helper/size_config.dart';

class DrawerPage extends StatelessWidget {
  const DrawerPage({Key? key, required this.model}) : super(key: key);

  final HomeNavPageModel model;

  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return Drawer(
      child: SafeArea(
        child: Column(
          children: [
            Container(
              height: 150,
              width: double.infinity,
              color: Colors.grey,
              child: Center(
                child: Image.asset(
                  "assets/images/app_logo.png",
                  height: SizeConfig.width * 0.15,
                  width: SizeConfig.width * 0.15,
                ),
              ),
            ),
            ListTile(
              leading: const Icon(Icons.local_offer_outlined),
              title: Text(locale.get("Products")),
              onTap: () {
                AppHelper.push(context, const MainPage(isFromHome: true));
              },
            ),
            ListTile(
              leading: const Icon(Icons.info_outline),
              title: Text(locale.get("About")),
              onTap: () {
                AppHelper.push(context, const AboutPage());
              },
            ),
            ListTile(
              title: Text(locale.get("Language")),
              subtitle: Text(model.appLanguage.languageName),
              leading: const Icon(Icons.language, color: Colors.grey),
              onTap: model.languageFun,
            ),
            ListTile(
                leading: const Icon(Icons.star_outline), title: Text(locale.get("Rate us")), onTap: model.rateUsFun),
            ListTile(
              leading: const Icon(Icons.lock_outline),
              title: Text(locale.get("Change password")),
              onTap: () {
                AppHelper.push(context, const ChangePasswordPage());
              },
            ),
            ListTile(
              title: Text(locale.get("Logout")),
              leading: const Icon(Icons.exit_to_app),
              onTap: () {
                auth.signOut();
                AppHelper.pushReplaceAll(context, const SplashPage());
              },
            ),
          ],
        ),
      ),
    );
  }
}
