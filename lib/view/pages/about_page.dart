import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassity/core/models/info.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/core/services/provider/provider_setup.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/widgets/components/main_image.dart';
import 'package:wassity/view/widgets/components/main_progress.dart';

class AboutPage extends StatelessWidget {
  const AboutPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    final bool isAr = locale.locale.languageCode == "ar";
    return BaseWidget<AboutPageModel>(
      model: AboutPageModel(context: context),
      builder: (_, model, child) {
        return Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.white,
            foregroundColor: Colors.black,
            title: Text(locale.get("About") , style: const  TextStyle(color: Colors.black),),
          ),
          body: model.busy
              ? const MainProgress()
              : SafeArea(
                  child: Container(
                    padding: const EdgeInsets.all(10),
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          MainImage(
                            height: SizeConfig.height * 0.2,
                            width: double.infinity,
                            imageType: ImageType.network,
                            radius: 5,
                            boxFit: BoxFit.cover,
                            imagePath:
                                "http://cryptoincome.io/wp-content/uploads/2018/01/DQmbtsqci8T9j9oNrQg4BxXr8hSsLYseiY788HuN7i7LfZu.jpg",
                          ),
                          const SizedBox(height: 10),
                          Text(
                            locale.get("Bio"),
                            style: const TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 24,
                            ),
                          ),
                          const SizedBox(height: 10),
                          Text(
                            isAr ? "${model.info.bioAr}" : "${model.info.bioEn}",
                          ),
                          Text(
                            locale.get("About Waseety"),
                            style: const TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 24,
                            ),
                          ),
                          const SizedBox(height: 10),
                          Text(
                            isAr ? "${model.info.aboutUsAr}" : "${model.info.aboutUsEn}",
                          ),
                          Text(
                            locale.get("Privacy policy"),
                            style: const TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 24,
                            ),
                          ),
                          const SizedBox(height: 10),
                          Text(
                            isAr ? "${model.info.policyAr}" : "${model.info.policyEn}",
                          ),
                          Center(
                              child: Stack(
                            alignment: Alignment.center,
                            children: [
                              SvgPicture.asset("assets/svgs/Group 290.svg"),
                              Image.asset(
                                "assets/images/app_logo.png",
                                height: SizeConfig.width * 0.20,
                                width: SizeConfig.width * 0.20,
                              ),
                            ],
                          )),
                        ],
                      ),
                    ),
                  ),
                ),
        );
      },
    );
  }
}

class AboutPageModel extends BaseModel {
  final BuildContext context;
  Info info = Info();
  AboutPageModel({required this.context}) {
    getInfo();
  }

  getInfo() async {
    setBusy();
    final res = await api.getInfo(context);
    if (res["success"] == true) {
      if (res["data"]["data"] != null) {
        info = Info.fromJson(res["data"]["data"]);
      }
    }
    setIdle();
  }
}
