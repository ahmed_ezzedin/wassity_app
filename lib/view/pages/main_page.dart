import 'package:flutter/material.dart';
import 'package:wassity/core/models/product.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/core/services/provider/provider_setup.dart';
import 'package:wassity/view/pages/about_page.dart';
import 'package:wassity/view/pages/auth/signin_page.dart';
import 'package:wassity/view/pages/nav_bar/home_nav_page.dart';
import 'package:wassity/view/styles/app_colors.dart';
import 'package:wassity/view/view_helper/app_helper.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/widgets/components/main_progress.dart';
import 'package:wassity/view/widgets/home_button.dart';
import 'package:wassity/view/widgets/product_item.dart';

class MainPage extends StatelessWidget {
  final bool isFromHome;
  const MainPage({Key? key, this.isFromHome = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return BaseWidget<MainPagePageModel>(
      model: MainPagePageModel(context: context),
      builder: (_, model, child) {
        return Scaffold(
          appBar: AppBar(
            // title: const Text("Main Page"),
            elevation: 0,
            title: Image.asset(
              "assets/images/app_logo.png",
              height: 50,
              width: 50,
            ),
          ),
          body: Container(
            width: double.infinity,
            padding: const EdgeInsets.all(10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  height: 100,
                  width: SizeConfig.width * 0.9,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.shade300,
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: const Offset(0, 0),
                      )
                    ],
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      HomeButton(
                        title: locale.get("Exchange"),
                        icon: Icons.repeat,
                        color: AppColors.primaryColor,
                        onTap: () async {
                          if (isFromHome) {
                            Navigator.of(context).pop();
                          } else {
                            if (auth.isLogin) {
                              auth.loadUser();
                              // bool isExpire = await auth.checkToken(auth.userModel!.expiresIn!);
                              bool isExpire = await auth.checkToken(40);
                              if (isExpire) {
                                AppHelper.push(context, const SigninPage());
                              } else {
                                AppHelper.pushReplaceAll(context, HomeNavPage());
                              }
                            } else {
                              AppHelper.push(context, const SigninPage());
                            }
                          }
                        },
                      ),
                      HomeButton(
                        title: locale.get("Products"),
                        icon: Icons.paste_rounded,
                        color: const Color(0xFFAA88CC),
                        onTap: () {},
                      ),
                      HomeButton(
                        title: locale.get("Info"),
                        icon: Icons.description,
                        color: const Color(0xFFAABBFF),
                        onTap: () {},
                      ),
                      HomeButton(
                        title: locale.get("About"),
                        icon: Icons.info,
                        color: const Color(0xFF11BBCC),
                        onTap: () {
                          AppHelper.push(context, const AboutPage());
                        },
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 10),
                Expanded(
                  child: model.busy
                      ? const MainProgress()
                      : GridView.builder(
                          itemCount: model.productList.length,
                          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            mainAxisSpacing: 10,
                            crossAxisSpacing: 10,
                            childAspectRatio: 0.8,
                          ),
                          itemBuilder: (context, index) {
                            return ProductItem(product: model.productList[index]);
                          },
                        ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

class MainPagePageModel extends BaseModel {
  final BuildContext context;

  List<Product> productList = [];
  MainPagePageModel({required this.context}) {
    getAllProducts();
  }
  getAllProducts() async {
    setBusy();
    final res = await api.getAllProducts(context);
    if (res["success"] == true) {
      if (res["data"]["data"] != null) {
        productList = res["data"]["data"].map<Product>((e) => Product.fromJson(e)).toList();
      }
    }
    setIdle();
  }
}
