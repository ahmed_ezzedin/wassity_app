// ignore_for_file: prefer_const_constructors
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:wassity/core/services/base/base_model.dart';
import 'package:wassity/core/services/base/base_widget.dart';
import 'package:wassity/core/services/core_helper/file_helper.dart';
import 'package:wassity/core/services/http/api.dart';
import 'package:wassity/core/services/localization/app_localization.dart';
import 'package:wassity/view/styles/text_styles.dart';
import 'package:wassity/view/view_helper/app_helper.dart';
import 'package:wassity/view/view_helper/dialog_helper.dart';
import 'package:wassity/view/view_helper/size_config.dart';
import 'package:wassity/view/widgets/buttons/main_button.dart';
import 'package:wassity/view/widgets/components/main_progress.dart';
import 'package:wassity/view/widgets/components/main_textfield.dart';
import 'package:http/http.dart' as http;

class AddProjectsPage extends StatelessWidget {
  const AddProjectsPage({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return BaseWidget<AddDealPageModel>(
      model: AddDealPageModel(context: context),
      builder: (_, model, child) {
        final locale = AppLocalizations.of(context);
        return Scaffold(
          body: SafeArea(
            child: Container(
              padding: const EdgeInsets.all(10),
              child: Form(
                autovalidateMode: model.autovalidateMode,
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: SizeConfig.height * 0.03),
                      Text(
                        locale.get("Start Building Your Deal"),
                        style: TextStyle(
                          fontWeight: FontWeight.w900,
                          fontSize: 24,
                        ),
                      ),
                      SizedBox(height: SizeConfig.height * 0.01),
                      Wrap(
                        alignment: WrapAlignment.center,
                        crossAxisAlignment: WrapCrossAlignment.center,
                        runAlignment: WrapAlignment.center,
                        children: [
                          Text(
                            locale.get(
                                "Start Your Project On Waseati Platform Quickly"),
                            style: TextStyle(
                              // fontWeight: FontWeight.w900,
                              // fontSize: 24,
                              color: Colors.grey,
                            ),
                          ),
                          SizedBox(width: 10),
                          SvgPicture.asset("assets/svgs/money (1).svg"),
                        ],
                      ),
                      SizedBox(height: SizeConfig.height * 0.05),
                      Text(
                        locale.get("Project Image"),
                        style: TextStyles.heading1Style,
                      ),
                      SizedBox(height: SizeConfig.height * 0.005),
                      InkWell(
                        child: Center(
                          child: Container(
                            height: SizeConfig.width * 0.5,
                            width: SizeConfig.width * 0.7,
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                            ),
                            child: model.pickedImage == null
                                ? Icon(
                                    Icons.image_outlined,
                                    size: 40,
                                    color: Colors.grey,
                                  )
                                : Image.file(model.pickedImage!),
                          ),
                        ),
                        onTap: () {
                          model.pickImage();
                        },
                      ),
                      SizedBox(height: SizeConfig.height * 0.01),
                      Text(
                        locale.get("Project title"),
                        style: TextStyles.heading1Style,
                      ),
                      SizedBox(height: SizeConfig.height * 0.005),
                      MainTextField(
                        controller: model.titleController,
                        hint: locale.get("Project title"),
                        validator: Validator.required,
                      ),
                      SizedBox(height: SizeConfig.height * 0.01),
                      Text(
                        locale.get("The Amount to be transferred"),
                        style: TextStyles.heading1Style,
                      ),
                      SizedBox(height: SizeConfig.height * 0.005),
                      MainTextField(
                        controller: model.amountController,
                        hint: locale.get("The Amount to be transferred"),
                        validator: Validator.number,
                        keyboardType: TextInputType.number,
                      ),
                      SizedBox(height: SizeConfig.height * 0.01),
                      Text(
                        locale.get("Details of the transactions"),
                        style: TextStyles.heading1Style,
                      ),
                      SizedBox(height: SizeConfig.height * 0.005),
                      MainTextField(
                        controller: model.descriptionController,
                        hint: locale.get("Details of the transactions"),
                        validator: Validator.required,
                        maxLines: 10,
                      ),
                      SizedBox(height: SizeConfig.height * 0.05),
                      model.busy
                          ? MainProgress()
                          : MainButton(
                              title: locale.get("Add A New Deal"),
                              onPressed: () {
                                model.addProject();
                              },
                            ),
                      SizedBox(height: SizeConfig.height * 0.02),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class AddDealPageModel extends BaseModel {
  final BuildContext context;

  AddDealPageModel({required this.context});

  final formKey = GlobalKey<FormState>();
  var autovalidateMode = AutovalidateMode.disabled;
  final titleController = TextEditingController();
  final amountController = TextEditingController();
  final descriptionController = TextEditingController();
  File? pickedImage;

  void submitFun() {
    if (formKey.currentState!.validate()) {
      addProject();
    } else {
      autovalidateMode = AutovalidateMode.always;
      setState();
    }
  }

  void pickImage() async {
    pickedImage = await FileHelper.pickImage(ImageSource.gallery);
    if (pickedImage != null) {
      setState();
    }
  }

  void addProject() async {
    setBusy();
    Uri uri = Uri.parse(EndPoints.serverURL + EndPoints.projects);
    var request = http.MultipartRequest('POST', uri)
      ..fields['title'] = titleController.text
      ..fields['amount'] = "${int.parse(amountController.text)}"
      ..fields['description'] = descriptionController.text
      ..files.add(await http.MultipartFile.fromPath(
        'photo',
        pickedImage!.path,
        // contentType: MediaType('application', 'x-tar'),
      ));
    request.headers.addAll(Headers.userAuth);
    var response = await request.send();
    if (response.statusCode == 200 || response.statusCode == 201) {
      AppHelper.appPrint('Uploaded!');
      DialogHelper.congratDialog(context);
    } else {
      AppHelper.appPrint('++++++++++++++++++++++++++++++');
      AppHelper.appPrint('${response.statusCode}');
    }
    setIdle();
  }
}

   // final res = await api.addProject(
    //   context,
    //   body: {
    //     "title": titleController.text,
    //     "amount": amountController.text,
    //     "description": titleController.text,
    //     "photo": pickedImage,
    //   },
    // );
    // print(res);
    // if(res!= null){
    //     if (res["success"]) {
    //     // Navigator.of(context).pop(true);
    //     DialogHelper.congratDialog(context);
    //   }
    // }